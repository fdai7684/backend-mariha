import { Keycloak } from 'keycloak-connect';
import { I_Order_Service } from '../models/order/order.model';
import { validateRequest } from '../services/validator.service';
import { CustomRouter } from './router.controller';

export class OrderRouter extends CustomRouter {
    
    service!: I_Order_Service;
    keycloak!: Keycloak;

    constructor(service: I_Order_Service, keycloak: Keycloak) {
        super (service, keycloak);
    }
    
    loadRoutes(): void {
        this.router.get('/orders', this.keycloak.protect('realm:customer'), validateRequest, this.service.getAllOrders);
        this.router.post('/order', this.keycloak.protect('realm:customer'), validateRequest, this.service.createOrder);
        this.router.get('/order/:orderId', this.keycloak.protect('realm:customer'), validateRequest, this.service.getOrderById);
        this.router.put('/order/:orderId', this.keycloak.protect('realm:customer'), validateRequest, this.service.updateOrderById);
        this.router.delete('/order/:orderId', this.keycloak.protect('realm:customer'), validateRequest, this.service.deleteOrderById);
    }

}
