import { Keycloak } from 'keycloak-connect';
import { I_User_Service } from '../models/user/user.model';
import { validateRequest } from '../services/validator.service';
import { CustomRouter } from './router.controller';

export class UserRouter extends CustomRouter {
    
    service!: I_User_Service;
    keycloak!: Keycloak;

    constructor(service: I_User_Service, keycloak: Keycloak) {
        super (service, keycloak);
    }
    
    loadRoutes(): void {    
        this.router.get('/users', this.keycloak.protect('realm:admin'), validateRequest, this.service.getAllUsers);
        this.router.post('/user', this.keycloak.protect('realm:customer'), validateRequest, this.service.createUser);
        this.router.get('/user', this.keycloak.protect('realm:customer'), validateRequest, this.service.getUserById);
        this.router.put('/user', this.keycloak.protect('realm:customer'), validateRequest, this.service.updateUserById);
        this.router.delete('/user', this.keycloak.protect('realm:customer'), validateRequest, this.service.deleteUserById);
    }

}
