import { Keycloak } from 'keycloak-connect';
import { config } from '../config/config';
import { CustomRouter } from './router.controller';
import { Service } from '../models/service.models';

export class AuthRouter extends CustomRouter {
    
    service?: Service;
    keycloak!: Keycloak;

    constructor(keycloak: Keycloak, service?: Service) {
        super (service, keycloak);
    }
    
    loadRoutes(): void {
        this.router.get('/login', this.keycloak.protect(), (req, res) => {res.redirect(config.FRONTEND_URL);});
        this.router.get('/account', (req, res) => {res.redirect(`${config.KEYCLOAK_AUTH_SERVER_URL}realms/bwma/account`);});
        // automatically handled by keycloak
        // this.router.get('/logout');
    }

}
