import { Keycloak } from 'keycloak-connect';
import { config } from '../config/config';
import { I_Cart_Service } from '../models/cart/cart.model';
import { validateRequest } from '../services/validator.service';
import { CustomRouter } from './router.controller';

export class CartRouter extends CustomRouter {
    
    service!: I_Cart_Service;
    keycloak!: Keycloak;

    constructor(service: I_Cart_Service, keycloak: Keycloak) {
        super (service, keycloak);
    }
    
    loadRoutes(): void {
        this.router.get('/', (req, res) => {res.send(config.VERSION);});
        this.router.get('/carts', this.keycloak.protect('realm:admin'), validateRequest, this.service.getAllCarts);
        this.router.post('/cart', this.keycloak.protect('realm:customer'), validateRequest, this.service.createCart);
        this.router.get('/cart/:cartId', this.keycloak.protect('realm:customer'), validateRequest, this.service.getCartById);
        this.router.put('/cart/:cartId', this.keycloak.protect('realm:customer'), validateRequest, this.service.updateCartById);
        this.router.delete('/cart/:cartId', this.keycloak.protect('realm:customer'), validateRequest, this.service.deleteCartById);
    }

}
