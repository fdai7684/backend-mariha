import { Keycloak } from 'keycloak-connect';
import { I_Product_Service } from '../models/product/product.model';
import { validateRequest } from '../services/validator.service';
import { CustomRouter } from './router.controller';

export class ProductRouter extends CustomRouter {
    
    service!: I_Product_Service;
    keycloak!: Keycloak;

    constructor(service: I_Product_Service, keycloak: Keycloak) {
        super (service, keycloak);
    }
    
    loadRoutes(): void {  
        this.router.get('/products', validateRequest, this.service.getAllProducts);
        this.router.post('/product', this.keycloak.protect('realm:admin'), validateRequest, this.service.createProduct);
        this.router.get('/product/:productId', validateRequest, this.service.getProductById);
        this.router.put('/product/:productId', this.keycloak.protect('realm:admin'), validateRequest, this.service.updateProductById);
        this.router.delete('/product/:productId', this.keycloak.protect('realm:admin'), validateRequest, this.service.deleteProductById);
    }

}
