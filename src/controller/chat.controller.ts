import { Keycloak } from 'keycloak-connect';
import { I_Chat_Service } from '../models/chat/chat.model';
import { validateRequest } from '../services/validator.service';
import { CustomRouter } from './router.controller';

export class ChatRouter extends CustomRouter {
    
    service!: I_Chat_Service;
    keycloak!: Keycloak;

    constructor(service: I_Chat_Service, keycloak: Keycloak) {
        super (service, keycloak);
    }
    
    loadRoutes(): void {
        this.router.get('/chats', this.keycloak.protect('realm:admin'), validateRequest, this.service.getAllChats);
        this.router.post('/chat', this.keycloak.protect(), validateRequest, this.service.createChat);
        this.router.get('/chat/:chatId', this.keycloak.protect(), validateRequest, this.service.getChatById);
        this.router.get('/chat/:chatId/messages', this.keycloak.protect(), validateRequest, this.service.getChatById);
        this.router.post('/chat/:chatId/message', this.keycloak.protect(), validateRequest, this.service.getChatById);
        
    }

}
