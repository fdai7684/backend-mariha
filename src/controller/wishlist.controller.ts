import { Keycloak } from 'keycloak-connect';
import { I_Wishlist_Service } from '../models/wishlist/wishlist.model';
import { validateRequest } from '../services/validator.service';
import { CustomRouter } from './router.controller';

export class WishlistRouter extends CustomRouter {
    
    service!: I_Wishlist_Service;
    keycloak!: Keycloak;

    constructor(service: I_Wishlist_Service, keycloak: Keycloak) {
        super (service, keycloak);
    }
    
    loadRoutes(): void {     
        this.router.get('/wishlists', this.keycloak.protect(), validateRequest, this.service.getAllWishlists);
        this.router.post('/wishlist', this.keycloak.protect('realm:customer'), validateRequest, this.service.createWishlist);
        this.router.get('/wishlist/:wishlistId', this.keycloak.protect('realm:customer'), validateRequest, this.service.getWishlistById);
        this.router.put('/wishlist/:wishlistId', this.keycloak.protect('realm:customer'), validateRequest, this.service.updateWishlistById);
        this.router.delete('/wishlist/:wishlistId', this.keycloak.protect('realm:customer'), validateRequest, this.service.deleteWishlistById);
    }

}
