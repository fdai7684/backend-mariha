import * as express from 'express';
import { Service } from '../models/service.models';
import { Keycloak } from 'keycloak-connect';

export abstract class CustomRouter {

    protected service?: Service;
    protected router: express.Router;
    protected keycloak?: Keycloak;

    constructor(service?: Service, keycloak?: Keycloak) {        
        this.router = express.Router({ mergeParams: true });
        this.service = service;
        this.keycloak = keycloak;
        this.loadRoutes();
    }

    abstract loadRoutes(): void;
  
    getRouter(): express.Router {
        return this.router;
    }
}
