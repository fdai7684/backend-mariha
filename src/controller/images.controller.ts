import { Keycloak } from 'keycloak-connect';
import { I_Image_Service } from '../models/images/image.model';
import { validateRequest } from '../services/validator.service';
import { CustomRouter } from './router.controller';

export class ImageRouter extends CustomRouter {
    
    service!: I_Image_Service;
    keycloak!: Keycloak;

    constructor(service: I_Image_Service, keycloak: Keycloak) {
        super (service, keycloak);
    }
    
    loadRoutes(): void {
        this.router.get('/images', this.keycloak.protect('realm:admin'), validateRequest, this.service.getAllImages);
        this.router.post('/image', this.keycloak.protect('realm:admin'), validateRequest, this.service.createImage);
        this.router.get('/image/:imageId', this.keycloak.protect('realm:admin'), validateRequest, this.service.getImageById);
        this.router.put('/image/:imageId', this.keycloak.protect('realm:admin'), validateRequest, this.service.updateImageById);
        this.router.delete('/image/:imageId', this.keycloak.protect('realm:admin'), validateRequest, this.service.deleteImageById);
    }

}
