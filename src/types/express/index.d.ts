import { KeycloakToken, KeyCloakTokenContent } from '../../models/keycloak/keycloak.models';

declare global {
    namespace Express {
        export interface Request {
            // Add your Keycloak-specific properties here
            kauth: {
                grant: {
                    access_token: KeycloakToken;
                    refresh_token: KeycloakToken;
                    id_token: KeycloakToken;
                };
                token_type: string;
                expires_in: number;
                __raw: string;
                store: unknown;
                unstore: unknown;
            };
            keycloakToken: KeyCloakTokenContent,
        }
    }
}