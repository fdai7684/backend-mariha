import { Request, Response, NextFunction } from 'express';
import { Service } from '../models/service.models';
import { CustomErrorHandler } from './error.service';
import { I_Wishlist_Database, I_Wishlist_Service } from '../models/wishlist/wishlist.model';

export class WishlistService extends Service implements I_Wishlist_Service {

    db!: I_Wishlist_Database;

    constructor(db: I_Wishlist_Database) {
        super(db);
    }
    
    getAllWishlists = async (req: Request, res: Response, next: NextFunction) => {
        const result = await this.db.getAllWishlistsDB();
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).send(result);
    };

    createWishlist = async (req: Request, res: Response, next: NextFunction) => {
        const result = await this.db.createWishlistDB();
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).json({ wishlistId: result.insertId });
    };
    
    getWishlistById = async (req: Request, res: Response, next: NextFunction) => {
        const { wishlistId } = req.body;
        const result = await this.db.getWishlistByIdDB(wishlistId);
        if (result instanceof CustomErrorHandler) return next (result);
        if (result.length !== 0) res.status(200).json(result[0]);
        else res.status(204).send();
    };

    deleteWishlistById = async (req: Request, res: Response, next: NextFunction) => {
        const { wishlistId } = req.body;
        const result = await this.db.deleteWishlistByIdDB(wishlistId);
        if (result instanceof CustomErrorHandler) return next (result);
        if (result === 1) res.status(200).send();
        else res.status(204).send();
    };

    
    updateWishlistById = async (req: Request, res: Response, next: NextFunction) => {
        const { wishlistId } = req.body;
        const result = await this.db.updateWishlistByIdDB(wishlistId);
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).json({ wishlistId });
    };
    
}