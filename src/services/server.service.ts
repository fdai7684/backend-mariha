import express, { Express } from 'express';
import { Server as NodeServer } from 'http';
import { handleRequestError } from './error.service';
import { config } from '../config/config';
import { RouteHandler } from '../models/server/server.models';
import { updateSchemas } from './validator.service';
import { Keycloak } from 'keycloak-connect';
import session, { MemoryStore } from 'express-session';
import { logger } from './logger.service';


export class Server {

    server!: NodeServer;
    keycloak: Keycloak;
    memoryStore: MemoryStore;
    app: Express;

    constructor(keycloak: Keycloak, memoryStore: MemoryStore) {
        this.app = express();
        this.keycloak = keycloak;
        this.memoryStore = memoryStore;
    }

    async startService(middlewares?: express.RequestHandler[], routes?: RouteHandler[]): Promise<void> {
        return new Promise((resolve, reject) => {
            resolve(this.load(middlewares, routes));
            reject(Error(`Couldn't start server`));
        });
    }

    private async load(middlewares?: express.RequestHandler[], routes?: RouteHandler[]) {
        this.addProcessHandler();
        this.initRouteHandler(middlewares);
        if (routes) this.initRoutes(routes);
        this.app.use(handleRequestError);
        await updateSchemas();
        await this.start();
    }

    private async start() {
        return new Promise((resolve, reject) => {
            this.server = this.app.listen(config.BACKEND_PORT, () => {
                logger.info(`Server ${config.APP_NAME} started on ${config.HOST}, listening on port ${config.BACKEND_PORT}`);
                resolve(0);
            }).on('error', (err) => reject(err));
        });
    }

    private initRoutes(routeHandlers: RouteHandler[]) {
        if (!routeHandlers) return;
        for (const handler of routeHandlers) {
            this.app.use(handler.route, handler.router.getRouter());
        }
    }

    private initRouteHandler(middlewares?: express.RequestHandler[]) {
        this.app.use(session({
            secret: 'somesecret',
            resave: false,
            saveUninitialized: true,
            store: this.memoryStore
          }));
        this.app.use(this.keycloak.middleware());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(express.json());
        if (!middlewares) return;
        for (const middleware of middlewares) {
            this.app.use(middleware);
        }
    }


    addProcessHandler(handlers?: [{ event: NodeJS.Signals, listener: NodeJS.SignalsListener }]) {
        process.once('SIGINT', this.closeApplication.bind(this));
        process.once('SIGTERM', this.closeApplication.bind(this));
        if (!handlers) return;
        for (const handler of handlers) {
            process.once(handler.event, handler.listener);
        }
    }

    private async closeApplication(signal: string | number) {
        logger.info(`${config.APP_NAME} => Received signal to terminate: ${signal}`);
        //await this.db.closeConnection();
        this.server.close();
        process.kill(process.pid, signal);
    }

}