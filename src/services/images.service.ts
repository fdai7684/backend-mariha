import { Request, Response, NextFunction } from 'express';
import { Service } from '../models/service.models';
import { CustomErrorHandler } from './error.service';
import { I_Image_Database, I_Image_Service } from '../models/images/image.model';

export class ImagesService extends Service implements I_Image_Service {

    db!: I_Image_Database;

    constructor(db: I_Image_Database) {
        super(db);
    }
    
    getAllImages = async (req: Request, res: Response, next: NextFunction) => {
        const result = await this.db.getAllImagesDB();
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).send(result);
    };

    createImage = async (req: Request, res: Response, next: NextFunction) => {
        const { url } = req.body;
        const result = await this.db.createImageDB(url);
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).json({ url, imagesId: result.insertId });
    };
    
    getImageById = async (req: Request, res: Response, next: NextFunction) => {
        const { imageId } = req.body;
        const result = await this.db.getImageByIdDB(imageId);
        if (result instanceof CustomErrorHandler) return next (result);
        if (result.length !== 0) res.status(200).json(result[0]);
        else res.status(204).send();
    };

    deleteImageById = async (req: Request, res: Response, next: NextFunction) => {
        const { imageId } = req.body;
        const result = await this.db.deleteImageByIdDB(imageId);
        if (result instanceof CustomErrorHandler) return next (result);
        if (result === 1) res.status(200).send();
        else res.status(204).send();
    };

    updateImageById = async (req: Request, res: Response, next: NextFunction) => {
        const { imageId, url } = req.body;
        const result = await this.db.updateImageByIdDB(imageId, url);
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).json({ imageId, url });
    };
}