import { Request, Response, NextFunction } from 'express';
import { Service } from '../models/service.models';
import { CustomErrorHandler } from './error.service';
import { I_User_Database, I_User_Service } from '../models/user/user.model';

// TODO: modify for querying keycloak
export class UserService extends Service implements I_User_Service {

    db!: I_User_Database;

    constructor(db: I_User_Database) {
        super(db);
    }

    getAllUsers = async (req: Request, res: Response, next: NextFunction) => {
        const result = await this.db.getAllUsersDB();
        if (result instanceof CustomErrorHandler) return next(result);
        res.status(200).send(result);
    };

    createUser = async (req: Request, res: Response, next: NextFunction) => {
        const { email, role, password, firstName, lastName, street, housenumber, additionalAddress, postcode, city, country } = req.body;
        // TODO: handle encryption
        const salt = '';
        const result = await this.db.createUserDB(email, role, password, salt, firstName, lastName, street, housenumber, additionalAddress, postcode, city, country);
        if (result instanceof CustomErrorHandler) return next(result);
        res.status(200).json({ email, role, password, salt, firstName, lastName, street, housenumber, additionalAddress, postcode, city, country, userId: result.insertId });
    };

    getUserById = async (req: Request, res: Response, next: NextFunction) => {
        const userId = req.keycloakToken.sub;
        const result = await this.db.getUserByIdDB(userId);
        if (result instanceof CustomErrorHandler) return next(result);
        if (result.length !== 0) res.status(200).json(result[0]);
        else res.status(204).send();
    };

    deleteUserById = async (req: Request, res: Response, next: NextFunction) => {
        const userId = req.keycloakToken.sub;
        const result = await this.db.deleteUserByIdDB(userId);
        if (result instanceof CustomErrorHandler) return next(result);
        if (result === 1) res.status(200).send();
        else res.status(204).send();
    };

    updateUserById = async (req: Request, res: Response, next: NextFunction) => {
        const userId = req.keycloakToken.sub;
        const { firstName, lastName, street, housenumber, additionalAddress, postcode, city, country } = req.body;
        const result = await this.db.updateUserByIdDB(userId, firstName, lastName, street, housenumber, additionalAddress, postcode, city, country);
        if (result instanceof CustomErrorHandler) return next(result);
        res.status(200).json({
            userId, firstName, lastName, street, housenumber, additionalAddress, postcode, city, country
        });
    };
}