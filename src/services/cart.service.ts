import { Request, Response, NextFunction } from 'express';
import { Service } from '../models/service.models';
import { CustomErrorHandler } from './error.service';
import { I_Cart_Database, I_Cart_Service } from '../models/cart/cart.model';

export class CartService extends Service implements I_Cart_Service {

    db!: I_Cart_Database;

    constructor(db: I_Cart_Database) {
        super(db);
    }
    
    getAllCarts = async (req: Request, res: Response, next: NextFunction) => {
        const result = await this.db.getAllCartsDB();
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).send(result);
    };

    createCart = async (req: Request, res: Response, next: NextFunction) => {
        const result = await this.db.createCartDB();
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).json({ cartId: result.insertId });
    };
    
    getCartById = async (req: Request, res: Response, next: NextFunction) => {
        const { cartId } = req.body;
        const result = await this.db.getCartByIdDB(cartId);
        if (result instanceof CustomErrorHandler) return next (result);
        if (result.length !== 0) res.status(200).json(result[0]);
        else res.status(204).send();
    };

    deleteCartById = async (req: Request, res: Response, next: NextFunction) => {
        const { cartId } = req.body;
        const result = await this.db.deleteCartByIdDB(cartId);
        if (result instanceof CustomErrorHandler) return next (result);
        if (result === 1) res.status(200).send();
        else res.status(204).send();
    };

  
    updateCartById = async (req: Request, res: Response, next: NextFunction) => {
        const { cartId } = req.body;
        const result = await this.db.updateCartByIdDB(cartId);
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).json({ cartId });
    };
}