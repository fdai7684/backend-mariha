import { NextFunction, Request, Response } from 'express';
import { logger } from './logger.service';


export class CustomErrorHandler extends Error {

    public code: number;
    public message: string;
    public publicMessage: string;

    constructor(code: number, message: string, publicMessage: string) {
        super(message);
        this.code = code;
        this.message = message;
        this.publicMessage = publicMessage;
    }
}

export function handleRequestError(err: CustomErrorHandler, req: Request, res: Response, next: NextFunction) {
    if (res.headersSent) return next(err.publicMessage);
    logger.error(err);
    res.status(err.code).send(err.publicMessage );
}
