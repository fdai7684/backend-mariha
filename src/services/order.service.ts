import { Request, Response, NextFunction } from 'express';
import { Service } from '../models/service.models';
import { CustomErrorHandler } from './error.service';
import { I_Order_Database, I_Order_Service } from '../models/order/order.model';

export class OrderService extends Service implements I_Order_Service {

    db!: I_Order_Database;

    constructor(db: I_Order_Database) {
        super(db);
    }
    
    getAllOrders = async (req: Request, res: Response, next: NextFunction) => {
        const result = await this.db.getAllOrdersDB();
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).send(result);
    };

    createOrder = async (req: Request, res: Response, next: NextFunction) => {
        const { paymentMethod, address, complete } = req.body;
        const result = await this.db.createOrderDB(paymentMethod, address, complete);
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).json({ paymentMethod, address, complete, orderId: result.insertId });
    };
    
    getOrderById = async (req: Request, res: Response, next: NextFunction) => {
        const { orderId } = req.body;
        const result = await this.db.getOrderByIdDB(orderId);
        if (result instanceof CustomErrorHandler) return next (result);
        if (result.length !== 0) res.status(200).json(result[0]);
        else res.status(204).send();
    };

    deleteOrderById = async (req: Request, res: Response, next: NextFunction) => {
        const { orderId } = req.body;
        const result = await this.db.deleteOrderByIdDB(orderId);
        if (result instanceof CustomErrorHandler) return next (result);
        if (result === 1) res.status(200).send();
        else res.status(204).send();
    };

    updateOrderById = async (req: Request, res: Response, next: NextFunction) => {
        const { orderId, paymentMethod, address, complete } = req.body;
        const result = await this.db.updateOrderByIdDB(orderId, paymentMethod, address, complete);
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).json({ orderId, paymentMethod, address, complete });
    };
}