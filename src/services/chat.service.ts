import { Request, Response, NextFunction } from 'express';
import { Service } from '../models/service.models';
import { CustomErrorHandler } from './error.service';
import { I_Chat_Database, I_Chat_Service } from '../models/chat/chat.model';

export class ChatService extends Service implements I_Chat_Service {

    db!: I_Chat_Database;

    constructor(db: I_Chat_Database) {
        super(db);
    }

    getAllChats = async (req: Request, res: Response, next: NextFunction) => {
        const result = await this.db.getAllChatsDB();
        if (result instanceof CustomErrorHandler) return next(result);
        res.status(200).send(result);
    };

    createChat = async (req: Request, res: Response, next: NextFunction) => {
        const userId = req.keycloakToken.sub;
        const result = await this.db.createChatDB(userId);
        if (result instanceof CustomErrorHandler) return next(result);
        res.status(200).json({ chatId: result.insertId });
    };

    getChatById = async (req: Request, res: Response, next: NextFunction) => {
        const { chatId } = req.body;
        const result = await this.db.getChatByIdDB(chatId);
        if (result instanceof CustomErrorHandler) return next(result);
        if (result.length !== 0) res.status(200).json(result[0]);
        else res.status(204).send();
    };

    getMessagesByChatId = async (req: Request, res: Response, next: NextFunction) => {
        const { chatId } = req.body;
        const result = await this.db.getMessagesByChatIdDB(chatId);
        if (result instanceof CustomErrorHandler) return next(result);
        res.status(200).send(result);
    };

    createMessageByChatId = async (req: Request, res: Response, next: NextFunction) => {
        const {content, chatId} = req.body;
        const result = await this.db.createMessageByChatIdDB(content, chatId);
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).json({ messageId: result.insertId, content, chatId });
    };
}