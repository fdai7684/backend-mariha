import { Request, Response, NextFunction } from 'express';
import { Service } from '../models/service.models';
import { CustomErrorHandler } from './error.service';
import { I_Product_Database, I_Product_Service } from '../models/product/product.model';

export class ProductService extends Service implements I_Product_Service {

    db!: I_Product_Database;

    constructor(db: I_Product_Database) {
        super(db);
    }
    
    getAllProducts = async (req: Request, res: Response, next: NextFunction) => {
        const result = await this.db.getAllProductsDB();
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).send(result);
    };

    createProduct = async (req: Request, res: Response, next: NextFunction) => {
        const { price, amount, createdAt } = req.body;
        const result = await this.db.createProductDB(price, amount);
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).json({ price, amount, createdAt, productId: result.insertId });
    };
    
    getProductById = async (req: Request, res: Response, next: NextFunction) => {
        const { productId } = req.body;
        const result = await this.db.getProductByIdDB(productId);
        if (result instanceof CustomErrorHandler) return next (result);
        if (result.length !== 0) res.status(200).json(result[0]);
        else res.status(204).send();
    };

    deleteProductById = async (req: Request, res: Response, next: NextFunction) => {
        const { productId } = req.body;
        const result = await this.db.deleteProductByIdDB(productId);
        if (result instanceof CustomErrorHandler) return next (result);
        if (result === 1) res.status(200).send();
        else res.status(204).send();
    };

    updateProductById = async (req: Request, res: Response, next: NextFunction) => {
        const { productId, price, amount } = req.body;
        const result = await this.db.updateProductByIdDB(productId, price, amount);
        if (result instanceof CustomErrorHandler) return next (result);
        res.status(200).json({ productId, price, amount });
    };
}