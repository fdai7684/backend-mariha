import session from 'express-session';
import Keycloak from 'keycloak-connect';
import { kcConfig } from '../config/keycloak';
import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import { KeyCloakTokenContent } from '../models/keycloak/keycloak.models';


const memoryStore = new session.MemoryStore();
const keycloak = new Keycloak({ store: memoryStore }, kcConfig);

const keycloakTokenDecoder = (req: Request, res: Response, next: NextFunction) => {    
    if (!req.kauth || !req.kauth.grant) return next();
    const decoded = jwt.decode(req.kauth.grant.access_token.token) as KeyCloakTokenContent;
    req.keycloakToken = decoded;
    next();
};


export { keycloak, memoryStore, keycloakTokenDecoder };
