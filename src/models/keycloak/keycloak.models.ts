export interface KeycloakToken { 
    token: string
    clientId: string | undefined,
    header: [unknown],
    content: [unknown],
    signature: Buffer,
    signed: string
}

export interface KeyCloakTokenContent {
    exp: number,
    iat: number,
    auth_time: number,
    jti: string,
    iss: string,
    aud: string,
    sub: string, // unique user identifier
    typ: string,
    azp: string,
    session_state: string,
    acr: string,
    string: [string],
    realm_access: { roles: [] },
    resource_access: { account: [unknown] },
    scope: string,
    sid: string,
    address: {
        street_address: string,
        postal_code: string,
        country: string
    },
    email_verified: false,
    house_number: string,
    city: string,
    name: string,
    preferred_username: string,
    given_name: string,
    family_name: string,
    email: string
}