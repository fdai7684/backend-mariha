import * as Joi from 'joi';
import { schemas } from '../../services/validator.service';

export const imageScheme = Joi.object({
    imageId: Joi.number(),
    url: Joi.string(),
    createdAt: Joi.string()
});

export const imageIdScheme = imageScheme.keys({
    imageId:Joi.number()
});

export const imageProductScheme = Joi.object({
    imageId: Joi.number(),
    productId: Joi.number()
});

schemas['/images'] = {
    GET: Joi.object({
        images: Joi.array().items(imageIdScheme)
    })
};
schemas['/image'] = {
    GET: imageIdScheme,
    POST: imageScheme,
    PUT: imageIdScheme,
    DELETE: Joi.object({
        imagesId: Joi.number()
    })
};