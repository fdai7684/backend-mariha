import { NextFunction, Request, Response } from 'express';
import { ResultSetHeader } from 'mysql2';
import { CustomErrorHandler } from '../../services/error.service';

import { Service } from '../service.models';
import { KnexDatabase } from '../../database/knex.database';

export interface I_Image  {
    url: string,
    createdAt: string
}

export interface I_Image_Id extends I_Image {
    imageId: number,
}

export interface I_Image_Image {
    imageId: number,
    productId: number,
}

export interface I_Image_Database  extends KnexDatabase{
    getAllImagesDB(): Promise<I_Image_Id[] | CustomErrorHandler>,
    createImageDB(url: string): Promise<ResultSetHeader | CustomErrorHandler>, 
    getImageByIdDB(imageId: number): Promise<I_Image_Id[] | CustomErrorHandler>
    deleteImageByIdDB(imageId: number): Promise<number | CustomErrorHandler>
    updateImageByIdDB(imageId: number, url: string): Promise<number | CustomErrorHandler>
}

export interface I_Image_Service extends Service {
    getAllImages(req: Request, res: Response, next: NextFunction): void
    createImage(req: Request, res: Response, next: NextFunction): void
    getImageById(req: Request, res: Response, next: NextFunction): void
    deleteImageById(req: Request, res: Response, next: NextFunction): void
    updateImageById(req: Request, res: Response, next: NextFunction): void
}