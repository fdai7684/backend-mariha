import * as Joi from 'joi';
import { dateTimeValidationHelper, schemas } from '../../services/validator.service';

export const orderScheme = Joi.object({
    status: Joi.string(),
    paymentMethod: Joi.string(),
    address: Joi.string(),
    complete: Joi.boolean(),
    createdAt: Joi.string().custom(dateTimeValidationHelper)
});

export const orderIdScheme = orderScheme.keys({
    orderId:Joi.number()
});

export const orderProductScheme = Joi.object({
    orderId: Joi.number(),
    productId: Joi.number(),
    amount: Joi.number(),
    pricePerPiece: Joi.number(),
});


export const userOrder = Joi.object({
    userId: Joi.string(),
    orderId: Joi.number()
});

schemas['/orders'] = {
    GET: Joi.object({
        orders: Joi.array().items(orderIdScheme)
    })
};
schemas['/order'] = {
    GET: orderIdScheme,
    POST: orderScheme,
    PUT: orderIdScheme,
    DELETE: Joi.object({
        ordersId: Joi.number()
    })
};