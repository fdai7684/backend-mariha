import { NextFunction, Request, Response } from 'express';
import { CustomErrorHandler } from '../../services/error.service';

import { ResultSetHeader } from 'mysql2';
import { Service } from '../service.models';
import { KnexDatabase } from '../../database/knex.database';

export interface I_Order {
    status: string,
    paymentMethod: string,
    address: string,
    complete: boolean,
    createdAt: string
}

export interface I_Order_Id extends I_Order {
    orderId: number,
}

export interface I_Order_Order {
    orderId: number,
    productId: number,
    amount: number,
    pricePerPiece: number,
}

export interface I_User_Order {
    userId: string,
    orderId: number
}

export interface I_Order_Database  extends KnexDatabase{
    getAllOrdersDB(): Promise<I_Order_Id[] | CustomErrorHandler>,
    createOrderDB(paymentMethod: string, address: string, complete: boolean): Promise<ResultSetHeader | CustomErrorHandler>, 
    getOrderByIdDB(orderId: number): Promise<I_Order_Id[] | CustomErrorHandler>
    deleteOrderByIdDB(orderId: number): Promise<number | CustomErrorHandler>
    updateOrderByIdDB(orderId: number, paymentMethod: string, address: string, complete: boolean): Promise<number | CustomErrorHandler>
}

export interface I_Order_Service extends Service {
    getAllOrders(req: Request, res: Response, next: NextFunction): void
    createOrder(req: Request, res: Response, next: NextFunction): void
    getOrderById(req: Request, res: Response, next: NextFunction): void
    deleteOrderById(req: Request, res: Response, next: NextFunction): void
    updateOrderById(req: Request, res: Response, next: NextFunction): void
}