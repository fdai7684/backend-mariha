import * as Joi from 'joi';
import { schemas } from '../../services/validator.service';

export const cartScheme = Joi.object({
    wishlist: Joi.boolean()
});

export const cartIdScheme = cartScheme.keys({
    cartId:Joi.number()
});

export const cartProductScheme = Joi.object({
    cartId: Joi.number(),
    productId: Joi.number(),
    amount: Joi.number()
});


schemas['/carts'] = {
    GET: Joi.object({
        images: Joi.array().items(cartIdScheme)
    })
};
schemas['/cart'] = {
    GET: cartIdScheme,
    POST: cartScheme,
    PUT: cartIdScheme,
    DELETE: Joi.object({
        cartId: Joi.number()
    })
};