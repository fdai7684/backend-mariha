import { NextFunction, Request, Response } from 'express';
import { ResultSetHeader } from 'mysql2';
import { CustomErrorHandler } from '../../services/error.service';

import { Service } from '../service.models';
import { KnexDatabase } from '../../database/knex.database';

export interface I_Cart  {
    wishlist: false
}

export interface I_Cart_Id extends I_Cart {
    cartId: number,
}

export interface I_Product_Cart {
    cartId: number,
    productId: number,
    amount: number
}

export interface I_Cart_Database  extends KnexDatabase {
    getAllCartsDB(): Promise<I_Cart_Id[] | CustomErrorHandler>,
    createCartDB(): Promise<ResultSetHeader | CustomErrorHandler>, 
    getCartByIdDB(cartId: number): Promise<I_Cart_Id[] | CustomErrorHandler>
    deleteCartByIdDB(cartId: number): Promise<number | CustomErrorHandler>
    updateCartByIdDB(cartId: number): Promise<number | CustomErrorHandler>
}

export interface I_Cart_Service extends Service {
    getAllCarts(req: Request, res: Response, next: NextFunction): void
    createCart(req: Request, res: Response, next: NextFunction): void
    getCartById(req: Request, res: Response, next: NextFunction): void
    deleteCartById(req: Request, res: Response, next: NextFunction): void
    updateCartById(req: Request, res: Response, next: NextFunction): void
}