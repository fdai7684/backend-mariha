import * as Joi from 'joi';

export const API_GET_OPTIONS_SCHEME = Joi.object({
    limit: Joi.number().integer().min(1).default(100),
    offset: Joi.number().integer().min(0).default(0),
    sortBy: Joi.string().min(1),
    sortOrder: Joi.string().valid('asc', 'desc').default('asc')
});
