import * as Joi from 'joi';
import { dateTimeValidationHelper, schemas } from '../../services/validator.service';

export const userScheme = Joi.object({
    email: Joi.string(),
    password: Joi.string(),
    salt: Joi.string(),
    role: Joi.string().valid('admin', 'customer'),
    firstName: Joi.string(),
    lastName: Joi.string(),
    street: Joi.string(),
    housenumber: Joi.string(),
    additionalAddress: Joi.string(),
    postcode: Joi.string(),
    city: Joi.string(),
    country: Joi.string(),
    createdAt: Joi.string().custom(dateTimeValidationHelper)
});

export const userIdScheme = userScheme.keys({
    userId: Joi.string(),
});


schemas['/users'] = {
    GET: Joi.object({
        users: Joi.array().items(userIdScheme)
    })
};
schemas['/user'] = {
    GET: userIdScheme,
    POST: userScheme,
    PUT: userIdScheme,
    DELETE: Joi.object({
        usersId: Joi.number()
    })
};