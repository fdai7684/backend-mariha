import { Request, Response, NextFunction } from 'express';
import { CustomErrorHandler } from '../../services/error.service';
import { Service } from '../service.models';
import { ResultSetHeader } from 'mysql2';
import { KnexDatabase } from '../../database/knex.database';

export type USER_ROLE = 'admin' | 'customer';

export interface I_Login {
    userId: string,
    email: string,
}

export type I_Register = I_Login;

export interface I_User_DB extends I_Login {
    password: string,
    salt: string,
    role: USER_ROLE,
    firstName: string,
    lastName: string,
    street: string,
    housenumber: string,
    additionalAddress: string,
    postcode: string,
    city: string,
    country: string,
    createdAt: Date
}

export interface I_User_Database  extends KnexDatabase{
    getAllUsersDB(): Promise<I_User_DB[] | CustomErrorHandler>,
    createUserDB(email: string,
        role: USER_ROLE,
        password: string,
        salt: string,
        firstName: string,
        lastName: string,
        street: string,
        housenumber: string,
        additionalAddress: string,
        postcode: string,
        city: string,
        country: string): Promise<ResultSetHeader | CustomErrorHandler>, 
    getUserByIdDB(userId: string): Promise<I_User_DB[] | CustomErrorHandler>
    deleteUserByIdDB(userId: string): Promise<number | CustomErrorHandler>
    updateUserByIdDB(userId: string,
        firstName: string,
        lastName: string,
        street: string,
        housenumber: string,
        additionalAddress: string,
        postcode: string,
        city: string,
        country: string): Promise<number | CustomErrorHandler>
}

export interface I_User_Service extends Service {
    getAllUsers(req: Request, res: Response, next: NextFunction): void
    createUser(req: Request, res: Response, next: NextFunction): void
    getUserById(req: Request, res: Response, next: NextFunction): void
    deleteUserById(req: Request, res: Response, next: NextFunction): void
    updateUserById(req: Request, res: Response, next: NextFunction): void
}
