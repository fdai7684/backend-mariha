import { NextFunction, Request, Response } from 'express';
import { ResultSetHeader } from 'mysql2';
import { CustomErrorHandler } from '../../services/error.service';

import { Service } from '../service.models';
import { KnexDatabase } from '../../database/knex.database';

export interface I_Chat  {
    wishlist: false
}

export interface I_Chat_Id extends I_Chat {
    chatId: number,
}

export interface I_Message {
    chatId: number,
    content: string
}

export interface I_Message_Id extends I_Message {
    messageId: number,
    createdAt: Date
}


export interface I_Chat_Database  extends KnexDatabase{
    getAllChatsDB(): Promise<I_Chat_Id[] | CustomErrorHandler>,
    createChatDB(userId: string): Promise<ResultSetHeader | CustomErrorHandler>, 
    getChatByIdDB(chatId: number): Promise<I_Chat_Id[] | CustomErrorHandler>
    getMessagesByChatIdDB(chatId: number): Promise<I_Message_Id[] | CustomErrorHandler>
    createMessageByChatIdDB(chatId: number, content: string): Promise<ResultSetHeader | CustomErrorHandler>
}

export interface I_Chat_Service extends Service {
    getAllChats(req: Request, res: Response, next: NextFunction): void
    createChat(req: Request, res: Response, next: NextFunction): void
    getChatById(req: Request, res: Response, next: NextFunction): void
    getMessagesByChatId(req: Request, res: Response, next: NextFunction): void
    createMessageByChatId(req: Request, res: Response, next: NextFunction): void
}