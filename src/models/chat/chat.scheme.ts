import * as Joi from 'joi';
import { schemas } from '../../services/validator.service';

export const chatScheme = Joi.object({
    userId: Joi.string()
});

export const chatIdScheme = chatScheme.keys({
    chatId:Joi.number()
});

schemas['/chats'] = {
    GET: Joi.object({})
};
schemas['/chat'] = {
    GET: chatIdScheme,
    POST: chatScheme
};
schemas['/chat/messages'] = {
    GET: Joi.object({
        chatId: Joi.number()
    })
};
schemas['/chat/message'] = {
    POST: Joi.object({
        chatId: Joi.number(),
        content: Joi.string()
    })
};