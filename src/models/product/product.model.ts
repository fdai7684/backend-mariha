import { Request, Response, NextFunction } from 'express';
import { CustomErrorHandler } from '../../services/error.service';
import { Service } from '../service.models';
import { ResultSetHeader } from 'mysql2';

import { KnexDatabase } from '../../database/knex.database';

export interface I_Product {
    price: number,
    amount: number,
    createdAt: Date
}

export interface I_Product_Id extends I_Product {
    productId: number,
}


export interface I_Product_Database  extends KnexDatabase{
    getAllProductsDB(): Promise<I_Product_Id[] | CustomErrorHandler>,
    createProductDB(price: number, amount: number): Promise<ResultSetHeader | CustomErrorHandler>, 
    getProductByIdDB(productId: number): Promise<I_Product_Id[] | CustomErrorHandler>
    deleteProductByIdDB(productId: number): Promise<number | CustomErrorHandler>
    updateProductByIdDB(productId: number, price: number, amount: number): Promise<number | CustomErrorHandler>
}

export interface I_Product_Service extends Service {
    getAllProducts(req: Request, res: Response, next: NextFunction): void
    createProduct(req: Request, res: Response, next: NextFunction): void
    getProductById(req: Request, res: Response, next: NextFunction): void
    deleteProductById(req: Request, res: Response, next: NextFunction): void
    updateProductById(req: Request, res: Response, next: NextFunction): void
}
