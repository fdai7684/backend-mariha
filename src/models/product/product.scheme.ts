import * as Joi from 'joi';
import { dateTimeValidationHelper, schemas } from '../../services/validator.service';

export const productScheme = Joi.object({
    price: Joi.number().precision(2),
    amount: Joi.number().integer().min(0),
    createdAt: Joi.string().custom(dateTimeValidationHelper)
});

export const productIdScheme = productScheme.keys({
    productId: Joi.number().integer().min(1)
});

schemas['/products'] = {
    GET: Joi.object({})
};
schemas['/product'] = {
    GET: productIdScheme,
    POST: productScheme,
    PUT: productIdScheme,
    DELETE: Joi.object({
        productsId: Joi.number()
    })
};