import { NextFunction, Request, Response } from 'express';
import { CustomErrorHandler } from '../../services/error.service';

import { ResultSetHeader } from 'mysql2';
import { Service } from '../service.models';
import { KnexDatabase } from '../../database/knex.database';

export interface I_Wishlist {
    wishlist: true
}

export interface I_Wishlist_Id extends I_Wishlist {
    cartId: number
}

export interface I_Wishlist_Wishlist {
    cartId: number,
    productId: number,
    amount: number
}

export interface I_Wishlist_Database  extends KnexDatabase{
    getAllWishlistsDB(): Promise<I_Wishlist_Id[] | CustomErrorHandler>,
    createWishlistDB(): Promise<ResultSetHeader | CustomErrorHandler>, 
    getWishlistByIdDB(wishlistId: number): Promise<I_Wishlist_Id[] | CustomErrorHandler>
    deleteWishlistByIdDB(wishlistId: number): Promise<number | CustomErrorHandler>
    updateWishlistByIdDB(wishlistId: number): Promise<number | CustomErrorHandler>
}

export interface I_Wishlist_Service extends Service {
    getAllWishlists(req: Request, res: Response, next: NextFunction): void
    createWishlist(req: Request, res: Response, next: NextFunction): void
    getWishlistById(req: Request, res: Response, next: NextFunction): void
    deleteWishlistById(req: Request, res: Response, next: NextFunction): void
    updateWishlistById(req: Request, res: Response, next: NextFunction): void
}