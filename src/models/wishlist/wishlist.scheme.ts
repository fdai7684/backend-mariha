import * as Joi from 'joi';
import { schemas } from '../../services/validator.service';
import { cartIdScheme, cartProductScheme, cartScheme } from '../cart/cart.scheme';

export const wishlistScheme = cartScheme;
export const wishlistIdScheme = cartIdScheme;

export const wishlistProductScheme = cartProductScheme;


schemas['/wishlists'] = {
    GET: Joi.object({
        wishlists: Joi.array().items(wishlistIdScheme)
    })
};
schemas['/wishlist'] = {
    GET: wishlistIdScheme,
    POST: wishlistScheme,
    PUT: wishlistIdScheme,
    DELETE: Joi.object({
        wishlistsId: Joi.number()
    })
};