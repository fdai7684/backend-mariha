import { Server } from './services/server.service';
import { CartRouter } from './controller/cart.controller';
import { ImageRouter } from './controller/images.controller';
import { OrderRouter } from './controller/order.controller';
import { ProductRouter } from './controller/product.controller';
import { UserRouter } from './controller/user.controller';
import { WishlistRouter } from './controller/wishlist.controller';
import { CartService } from './services/cart.service';
import { ImagesService } from './services/images.service';
import { OrderService } from './services/order.service';
import { ProductService } from './services/product.service';
import { UserService } from './services/user.service';
import { WishlistService } from './services/wishlist.service';
import { CartDatabase } from './database/cart.database';
import { ImageDatabase } from './database/images.database';
import { OrderDatabase } from './database/order.database';
import { ProductDatabase } from './database/product.database';
import { UserDatabase } from './database/user.database';
import { WishlistDatabase } from './database/wishlist.database';
import { keycloak, keycloakTokenDecoder, memoryStore } from './services/keycloak.service';
import { AuthRouter } from './controller/auth.controller';

const server = new Server(keycloak, memoryStore);

const authRouter = new AuthRouter(keycloak);
const cartRouter = new CartRouter(new CartService(new CartDatabase()), keycloak);
const imageRouter = new ImageRouter(new ImagesService(new ImageDatabase()), keycloak);
const orderRouter = new OrderRouter(new OrderService(new OrderDatabase()), keycloak);
const productRouter = new ProductRouter(new ProductService(new ProductDatabase()), keycloak);
const userRouter = new UserRouter(new UserService(new UserDatabase()), keycloak);
const wishlistRouter = new WishlistRouter(new WishlistService(new WishlistDatabase()), keycloak);

void server.startService([keycloakTokenDecoder], [
    { route: '/', router: authRouter },
    { route: '/', router: cartRouter },
    { route: '/', router: imageRouter },
    { route: '/', router: orderRouter },
    { route: '/', router: productRouter },
    { route: '/', router: userRouter },
    { route: '/', router: wishlistRouter },
]);
