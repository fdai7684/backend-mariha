import type { Knex } from 'knex';


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('cartProducts', (table) => {
        table.integer('cartId').unsigned().notNullable(),
            table.integer('productId').unsigned().notNullable(),
            table.integer('amount').notNullable(),
            table.foreign('productId').references('productId').inTable('products');
            table.foreign('cartId').references('cartId').inTable('carts');
    });
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists('cartProducts');
}

