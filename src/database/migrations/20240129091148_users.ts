import type { Knex } from 'knex';


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('users', (table) => {
        table.string('userId').primary(),
        table.string('email').notNullable().unique(),
        table.string('password').notNullable(),
        table.string('salt').notNullable(),
        table.string('role').notNullable(),
        table.string('street').notNullable(),
        table.string('housenumber').notNullable(),
        table.string('additionalAddress').notNullable(),
        table.string('postcode').notNullable(),
        table.string('city').notNullable(),
        table.string('country').notNullable(),
        table.timestamp('createdAt').notNullable();
    });
}


export async function down(knex: Knex): Promise<void> { 
 return knex.schema.dropTableIfExists('users');
}

