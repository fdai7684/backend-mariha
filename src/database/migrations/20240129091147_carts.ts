import type { Knex } from 'knex';


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('carts', (table) => {
        table.increments('cartId').primary(),
            table.boolean('wishlist').notNullable();
    });
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists('carts');
}

