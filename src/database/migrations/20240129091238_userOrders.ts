import type { Knex } from 'knex';


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('userOrders', (table) => {
        table.integer('orderId').unsigned().notNullable(),
        table.string('userId').notNullable(),
        table.foreign('orderId').references('orderId').inTable('orders'),
            table.foreign('userId').references('userId').inTable('users');
    });
}


export async function down(knex: Knex): Promise<void> { 
 return knex.schema.dropTableIfExists('orders');
}

