import type { Knex } from 'knex';


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('productOrders', (table) => {
        table.integer('productId').unsigned().notNullable(),
        table.integer('orderId').unsigned().notNullable(),
        table.foreign('productId').references('productId').inTable('products');
        table.foreign('orderId').references('orderId').inTable('orders'),
        table.float('pricePerPiece').notNullable(),
        table.integer('amount');
    });
}


export async function down(knex: Knex): Promise<void> { 
 return knex.schema.dropTableIfExists('productOrders');
}

