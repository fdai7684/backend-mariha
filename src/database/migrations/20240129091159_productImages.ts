import type { Knex } from 'knex';


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('productImages', (table) => {
        table.integer('productId').unsigned().notNullable(),
        table.integer('imageId').unsigned().notNullable(),
        table.foreign('imageId').references('imageId').inTable('images'),
        table.foreign('productId').references('productId').inTable('products');
    });
}


export async function down(knex: Knex): Promise<void> { 
 return knex.schema.dropTableIfExists('productImages');

}

