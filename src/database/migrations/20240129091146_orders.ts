import type { Knex } from 'knex';


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('orders', (table) => {
        table.increments('orderId').primary(),
        table.string('status').notNullable(),
        table.string('paymentMethod').notNullable(),
        table.string('address').notNullable(),
        table.timestamp('createdAt').notNullable();
    });
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists('orders');
}

