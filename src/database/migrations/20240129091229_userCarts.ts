import type { Knex } from 'knex';


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('userCarts', (table) => {
        table.string('userId').notNullable(),
        table.integer('cartId').unsigned().notNullable(),
        table.foreign('cartId').references('cartId').inTable('carts'),
            table.foreign('userId').references('userId').inTable('users');
    });
}


export async function down(knex: Knex): Promise<void> { 
 return knex.schema.dropTableIfExists('userCarts');
}

