import type { Knex } from 'knex';


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('products', (table) => {
        table.increments('productId').primary();
        table.float('price').notNullable(),
        table.integer('amount'),
        table.timestamp('createdAt');
    });
}


export async function down(knex: Knex): Promise<void> { 
 return knex.schema.dropTableIfExists('products');
}

