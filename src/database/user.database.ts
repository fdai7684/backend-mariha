

import {I_User_DB, I_User_Database, USER_ROLE} from '../models/user/user.model';
import { ResultSetHeader } from 'mysql2';
import { CustomErrorHandler } from '../services/error.service';
import { KnexDatabase } from './knex.database';

export class UserDatabase extends KnexDatabase implements I_User_Database {
    async getAllUsersDB(): Promise<CustomErrorHandler | I_User_DB[]> {
        return this.db('users'); 
    }
    async createUserDB(email: string, role: USER_ROLE, password: string, salt: string, firstName: string, lastName: string, street: string, housenumber: string, additionalAddress: string, postcode: string, city: string, country: string): Promise<CustomErrorHandler | ResultSetHeader> {
        return this.db('users').insert({email, role, password, salt, firstName, lastName, street, housenumber, additionalAddress, postcode, city, country});
    }
    async getUserByIdDB(userId: string): Promise<CustomErrorHandler | I_User_DB[]> {
        return this.db('users').where(userId);
    }
    async deleteUserByIdDB(userId: string): Promise<number | CustomErrorHandler> {
        return this.db('users').where(userId).delete();
    }
    async updateUserByIdDB(userId: string, firstName: string, lastName: string, street: string, housenumber: string, additionalAddress: string, postcode: string, city: string, country: string): Promise<number | CustomErrorHandler> {
        return this.db('users').update({firstName, lastName, street, housenumber, additionalAddress, postcode, city, country}).where(userId);
    }
}