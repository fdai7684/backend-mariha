
import {I_Chat_Database, I_Chat_Id, I_Message_Id} from '../models/chat/chat.model';
import { ResultSetHeader } from 'mysql2';
import { CustomErrorHandler } from '../services/error.service';
import { KnexDatabase } from './knex.database';

export class ChatDatabase extends KnexDatabase implements I_Chat_Database {
    
    async getMessagesByChatIdDB(chatId: number): Promise<CustomErrorHandler | I_Message_Id[]> {
        return this.db('chats').where('chatId', chatId);
    }

    async createMessageByChatIdDB(chatId: number, content: string): Promise<CustomErrorHandler | ResultSetHeader> {
        return this.db('chats').insert({chatId, content});
    }
    
    async getAllChatsDB(): Promise<CustomErrorHandler | I_Chat_Id[]> {
        return this.db('chats');
    }
    async createChatDB(userId: string): Promise<CustomErrorHandler | ResultSetHeader> {
        return this.db('chats').insert({userId});
    }
    async getChatByIdDB(chatId: number): Promise<CustomErrorHandler | I_Chat_Id[]> {
        return this.db('chats').where('chatId', chatId);
    }

}