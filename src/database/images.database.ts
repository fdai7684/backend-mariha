
import {I_Image_Database, I_Image_Id} from '../models/images/image.model';
import { ResultSetHeader } from 'mysql2';
import { CustomErrorHandler } from '../services/error.service';
import { KnexDatabase } from './knex.database';

export class ImageDatabase extends KnexDatabase implements I_Image_Database {
    async getAllImagesDB(): Promise<CustomErrorHandler | I_Image_Id[]> {
        return this.db('images');
    }
    async createImageDB(url: string): Promise<CustomErrorHandler | ResultSetHeader> {
        return this.db('images').insert({url});
    }
    async getImageByIdDB(imageId: number): Promise<CustomErrorHandler | I_Image_Id[]> {
        return this.db('images').where('imageId', imageId);
    }
    async deleteImageByIdDB(imageId: number): Promise<number | CustomErrorHandler> {
        return this.db('images').where('imageId', imageId).del();
    }
    async updateImageByIdDB(imageId: number, url: string): Promise<number | CustomErrorHandler> {
        return this.db('images').update({url}).where(imageId);
    }
}