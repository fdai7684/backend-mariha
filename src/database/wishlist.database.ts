import {I_Wishlist_Database, I_Wishlist_Id} from '../models/wishlist/wishlist.model';
import { ResultSetHeader } from 'mysql2';
import { CustomErrorHandler } from '../services/error.service';
import { KnexDatabase } from './knex.database';

export class WishlistDatabase extends KnexDatabase implements I_Wishlist_Database {
    async getAllWishlistsDB(): Promise<CustomErrorHandler | I_Wishlist_Id[]> {
        return this.db('wishlist');
    }
    async createWishlistDB(): Promise<CustomErrorHandler | ResultSetHeader> {
        return this.db('wishlist').insert({});
    }
    async getWishlistByIdDB(cartId: number): Promise<CustomErrorHandler | I_Wishlist_Id[]> {
        return this.db('wishlist').where(cartId);
    }
    async deleteWishlistByIdDB(cartId: number): Promise<number | CustomErrorHandler> {
        return this.db('wishlist').where(cartId).delete();
    }
    async updateWishlistByIdDB(cartId: number): Promise<number | CustomErrorHandler> {
        throw new Error('Not yet implemented');
        return this.db('wishlist').update({}).where(cartId);
    }
}