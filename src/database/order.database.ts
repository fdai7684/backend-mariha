
import {I_Order_Database, I_Order_Id} from '../models/order/order.model';
import { ResultSetHeader } from 'mysql2';
import { CustomErrorHandler } from '../services/error.service';
import { KnexDatabase } from './knex.database';

export class OrderDatabase extends KnexDatabase implements I_Order_Database {
    async getAllOrdersDB(): Promise<CustomErrorHandler | I_Order_Id[]> {
        return this.db('orders');
    }
    async createOrderDB(paymentMethod: string, address: string, complete: boolean): Promise<CustomErrorHandler | ResultSetHeader> {
        return this.db('orders').insert({paymentMethod, address, complete});
    }
    async getOrderByIdDB(orderId: number): Promise<CustomErrorHandler | I_Order_Id[]> {
        return this.db('orders').where(orderId);
    }
    async deleteOrderByIdDB(orderId: number): Promise<number | CustomErrorHandler> {
        return this.db('orders').where(orderId).del();
    }
    async updateOrderByIdDB(orderId: number, paymentMethod: string, address: string, complete: boolean): Promise<number | CustomErrorHandler> {
        return this.db('orders').update({paymentMethod, address, complete}).where(orderId);
    }
}