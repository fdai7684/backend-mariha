import { Knex, knex } from 'knex';
import * as knexfile from './knexfile';
 
export const db = knex(knexfile);

export class KnexDatabase {
    
    db: Knex;

    constructor() {
        this.db = knex(knexfile);
    }
}