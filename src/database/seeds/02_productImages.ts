import { Knex } from 'knex';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('productImages').del();


    // Inserts seed entries
    await knex('productImages').insert([
        { productId: 1, imageId: 1 },
        { productId: 2, imageId: 2 },
        { productId: 3, imageId: 3 }
    ]);

}
