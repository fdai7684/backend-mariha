import { Knex } from 'knex';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('orders').del();

    // Inserts seed entries
    await knex('orders').insert([
        { orderId: 1, status: 'in transit', paymentMethod: 'visa', address: 'Germany, Frankfurt, Adenauerplatz 123' },
        { orderId: 2, status: 'fullfilled', paymentMethod: 'visa', address: 'Germany, Berlin, Berlinerplatz 12' },
        { orderId: 3, status: 'shipped', paymentMethod: 'mastercard', address: 'England, London, Westminster Abby 97' },
    ]);
}
