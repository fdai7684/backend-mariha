import { Knex } from 'knex';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('users').del();

    // Inserts seed entries
    await knex('users').insert([
        { userId: '1', email: 'max@muster.de', password: '', salt: '', role: 'customer', street: 'Musterstrasse', housenumber: '12', additionalAddress: '', postcode: '12345', city: 'Musterstadt', country: 'Germany' },
        { userId: '2', email: 'joyce@muster.de', password: '', salt: '', role: 'customer', street: 'Frankfurterstrasse', housenumber: '98', additionalAddress: '', postcode: '346543', city: 'Fulda', country: 'Germany' },
        { userId: '3', email: 'hans@noclue.de', password: '', salt: '', role: 'customer', street: 'Konrad-Adenauerstrasse', housenumber: '2', additionalAddress: '', postcode: '987', city: 'Frankfurt', country: 'Germany' },
        { userId: '4', email: 'margret@maerchen.com', password: '', salt: '', role: 'customer', street: 'Berliner Platz', housenumber: '1', additionalAddress: '', postcode: '34518', city: 'Berlin', country: 'Germany' },
        { userId: '5', email: 'john@doe.uk', password: '', salt: '', role: 'customer', street: 'Westminster Abby', housenumber: '3', additionalAddress: '', postcode: '98765', city: 'London', country: 'England' },
    ]);
}
