import { Knex } from 'knex';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('products').del();

    // Inserts seed entries
    await knex('products').insert([
        { productId: 1, price: 12.99, amount: 1973 },
        { productId: 2, price: 11.99, amount: 1987 },
        { productId: 3, price: 10.99, amount: 14 },
        { productId: 4, price: 1.99, amount: 987 },
        { productId: 5, price: 2.99, amount: 123 },
        { productId: 6, price: 6.99, amount: 345 },
        { productId: 7, price: 19.99, amount: 12 },
        { productId: 8, price: 99.99, amount: 40},
    ]);
}
