import { Knex } from 'knex';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('userCarts').del();

    // Inserts seed entries
    await knex('userCarts').insert([
        { userId: '1', cartId: 1 },
        { userId: '2', cartId: 2 },
        { userId: '3', cartId: 3 }
    ]);
}
