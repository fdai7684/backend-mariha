import { Knex } from 'knex';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('cartProducts').del();

    // Inserts seed entries
    await knex('cartProducts').insert([
        { cartId: 1, productId: 1, amount: 1 },
        { cartId: 2, productId: 1, amount: 2 },
        { cartId: 1, productId: 3, amount: 3 },
    ]);
}
