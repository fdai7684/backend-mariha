import { Knex } from 'knex';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('userOrders').del();

    // Inserts seed entries
    await knex('userOrders').insert([
        { userId: '1', orderId: 1 },
        { userId: '2', orderId: 2 },
        { userId: '3', orderId: 3 }
    ]);

    // enable foreign key checks
    await knex.raw('SET FOREIGN_KEY_CHECKS=1;');
}
