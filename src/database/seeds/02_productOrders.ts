import { Knex } from 'knex';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('productOrders').del();

    // Inserts seed entries
    await knex('productOrders').insert([
        { productId: 1, orderId: 1, pricePerPiece: 12.99, amount: 1 },
        { productId: 2, orderId: 1, pricePerPiece: 19.99, amount: 2 },
        { productId: 3, orderId: 2, pricePerPiece: 3.99, amount: 1 },
    ]);
}
