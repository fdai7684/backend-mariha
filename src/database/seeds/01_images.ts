import { Knex } from 'knex';

export async function seed(knex: Knex): Promise<void> {
    // disable foreign key check for deletes
    await knex.raw('SET FOREIGN_KEY_CHECKS=0;');

    // Deletes ALL existing entries
    await knex('images').del();

    // Inserts seed entries
    await knex('images').insert([
        { imageId: 1, url: 'https://www.americanexpress.com/de-de/amexcited/media/cache/default/cms/2022/01/Schwarz-Weiss-Fotografie-Titelbild-scaled.jpg' },
        { imageId: 2, url: 'https://data.wdr.de/ddj/deepfake-quiz-erkennen-sie-alle-ki-bilder/Titelbild.jpg' },
        { imageId: 3, url: 'https://media.istockphoto.com/id/944812540/de/foto/berglandschaft-ponta-delgada-insel-azoren.jpg?s=612x612&w=0&k=20&c=Q72FqanfThl0GXLgzHmGJOBmxAgF1bkRhGRHIEPNSNo=' },
    ]);
}
