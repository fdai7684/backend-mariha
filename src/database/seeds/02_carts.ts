import { Knex } from 'knex';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('carts').del();

    // Inserts seed entries
    await knex('carts').insert([
        { cartId: 1, wishlist: true },
        { cartId: 2, wishlist: false },
        { cartId: 3, wishlist: false },
    ]);
}
