import { ResultSetHeader } from 'mysql2';
import { I_Product_Id, I_Product_Database } from '../models/product/product.model';
import { CustomErrorHandler } from '../services/error.service';

import { KnexDatabase } from './knex.database';

export class ProductDatabase extends KnexDatabase implements I_Product_Database {

    async getAllProductsDB(): Promise<I_Product_Id[] | CustomErrorHandler> {
        return this.db('products') as Promise<I_Product_Id[] | CustomErrorHandler>;
    }
    async createProductDB(price: number, amount: number): Promise<ResultSetHeader | CustomErrorHandler> {
        return this.db('products').insert({price, amount});
    }
    async getProductByIdDB(productId: number): Promise<I_Product_Id[] | CustomErrorHandler> {
        return this.db('products').where(productId);
    }
    async deleteProductByIdDB(productId: number): Promise<number | CustomErrorHandler> {
        return this.db('products').where(productId).delete();
    }
    async updateProductByIdDB(productId: number, price: number, amount: number): Promise<number | CustomErrorHandler> {
        return this.db('products').update({price, amount}).where(productId);
    }
}