import { Connection, Pool, PoolConnection, PoolOptions, createPool } from 'mysql2';
import { config } from '../config/config';
import { DB, DbQueryResult, I_DB_Settings, DBReturn } from '../models/database/database.models';
import { CustomErrorHandler } from '../services/error.service';

export class MySQLDBController implements DB {

    private _settings: I_DB_Settings;
    private _pool: Pool;
    private _connection!: Connection;

    constructor(host?: string, user?: string, password?: string, database?: string, port?: number, connectionLimit?: string | number, options?: PoolOptions) {
        this._settings = this._createDBConfig(host ?? config.DB_HOST, user ?? config.DB_USER, database ?? config.DB_NAME, password ?? config.DB_PASSWORD, port ?? config.DB_PORT, connectionLimit ?? config.DB_CONNECTION_LIMIT, options);
        this._pool = createPool(this._settings);
        this._pool.getConnection(this.getConnectionCallback);
    }

    private getConnectionCallback = async (err: NodeJS.ErrnoException | null, connection: PoolConnection) => {
        if (err) throw new Error(`Couldn't connect to database\n${err}`);
        else this._connection = connection;
    };

    /**
     * Create a connection to mysql db.
     * @returns mysqldb connection object
     */
    async getConnection(): Promise<Connection> {
        return this._connection;
    }

    /**
     * Close connection to mysql db.
     * @returns Nothing if succesful, error if something went wrong.
     */
    async closeConnection(): Promise<void | Error> {
        if (!this._connection && !this._pool) return;
        if (this._connection) this._connection.end();
        if (this._pool) this._pool.end();
        return;
    }

    /**
     * Create an unchangeable object with the database settings.
     * @param host - database host address (e.g. localhost or 192.168.178.123)
     * @param user - database user to connect with
     * @param password - password for the user/ database
     * @param port - database connection port
     * @param connectionLimit - limit for possible connections to the database
     * @returns - database config object
     */
    private _createDBConfig(host: string, user: string, database: string, password: string, port: string | number, connectionLimit: string | number, options?: PoolOptions): I_DB_Settings {
        if (!user || !password || !database || !host || !port || !connectionLimit) throw new Error('Database config is missing information');
        if (typeof port === 'string') port = Number(port);
        if (typeof connectionLimit === 'string') connectionLimit = Number(connectionLimit);
        if (!options) return Object.freeze({ user, password, host, port, connectionLimit, database });
        return Object.freeze({ user, password, host, port, connectionLimit, database, ...options });
    }

    /**
     * Query the Database.
     * @param sql - sql query
     * @param options - parameters to insert into the query
     * @returns - query response of generic datatype 'T'
     */
    async query<T>(sql: string, options?: unknown): Promise<DBReturn<T> | CustomErrorHandler> {
        try {
            const [rows] = this._pool.query(sql, options) as unknown as DbQueryResult<T[]>;
            return rows;
        
        } catch (error) {
            return new CustomErrorHandler(500, `Error trying to write to database\n${error}`, 'Please try again or contact an admin if the problem persists.');
        }
    }
}