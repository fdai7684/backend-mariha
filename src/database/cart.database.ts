
import {I_Cart_Database, I_Cart_Id} from '../models/cart/cart.model';
import { ResultSetHeader } from 'mysql2';
import { CustomErrorHandler } from '../services/error.service';
import { KnexDatabase } from './knex.database';

export class CartDatabase extends KnexDatabase implements I_Cart_Database {
    
    async getAllCartsDB(): Promise<CustomErrorHandler | I_Cart_Id[]> {
        return this.db('carts');
    }
    async createCartDB(): Promise<CustomErrorHandler | ResultSetHeader> {
        return this.db('carts').insert({});
    }
    async getCartByIdDB(cartId: number): Promise<CustomErrorHandler | I_Cart_Id[]> {
        return this.db('carts').where('chatId', cartId);
    }
    async deleteCartByIdDB(cartId: number): Promise<number | CustomErrorHandler> {
        return this.db('carts').where('cartId', cartId).delete();
    }
    async updateCartByIdDB(cartId: number): Promise<number | CustomErrorHandler> {
        throw new Error('Not yet implemented');
        return this.db('carts').update({}).where(cartId);
    }

}