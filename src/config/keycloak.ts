import { config } from "./config";

export const kcConfig: any  = {
  "realm": "bwma",
  "auth-server-url": config.KEYCLOAK_AUTH_SERVER_URL,
  "ssl-required": "external",
  "resource": "nodejs-backend",
  "credentials": {
    "secret": config.KEYCLOAK_SECRET
  },
  "confidential-port": 0
}