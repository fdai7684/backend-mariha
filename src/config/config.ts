/* eslint-disable @typescript-eslint/no-var-requires */

import * as dotenv from 'dotenv';
import * as path from 'path';

dotenv.config({path: path.join(__dirname, '..', '..', 'development.env')});

export const config = {
    APP_NAME: require(path.join(__dirname, '..', '..', 'package.json')).name,
    VERSION: require(path.join(__dirname, '..', '..', 'package.json')).version,
    HOST: process.env.HOST ?? 'localhost',
    BACKEND_PORT: process.env.BACKEND_PORT ?? 8081, 
    FRONTEND_URL: "http://localhost",
    DB_HOST: process.env.DB_HOST || 'localhost',
    DB_USER: process.env.DB_USER || 'bwma',
    DB_PASSWORD: process.env.DB_PASSWORD || 'test',
    DB_NAME: process.env.DB_NAME || 'adventuresphere',
    DB_PORT: process.env.DB_PORT || 3306,
    DB_CONNECTION_LIMIT: process.env.DB_CONNECTION_LIMIT || 5,
    KEYCLOAK_AUTH_SERVER_URL: process.env.KEYCLOAK_AUTH_SERVER_URL || "http://127.0.0.1:8180/",
    KEYCLOAK_SECRET: process.env.KEYCLOAK_SECRET || "nQfSk19MyAPzkwWHzjxEzMcOxwhQaNLA",
    LOG_FOLDER_PATH: path.join(__dirname, '..', '..', 'logs'), //default path, If changed add log folder to gitignore
    NODE_ENV: process.env.NODE_ENV || "development",
};