'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
// Sync object
const config = {
    verbose: true,
    transform: {
        '^.+\\.tsx?$': 'ts-jest',
    },
    rootDir: '../',
    preset: 'ts-jest',
    testEnvironment: 'node',
    coverageDirectory: './coverage',
    collectCoverageFrom: ['**/src/**', '!**/node_modules/**', '!**/src/database/migrations/**'],
    coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
    testMatch: ['**/tests/**']
};
exports.default = config;
