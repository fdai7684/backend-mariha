# create databases
CREATE DATABASE IF NOT EXISTS `keycloak`;
CREATE DATABASE IF NOT EXISTS `bwma`;

CREATE USER 'keycloak'@'localhost' IDENTIFIED BY 'keycloak';
GRANT ALL PRIVILEGES ON *.* TO 'keycloak'@'%';

CREATE USER 'bwma'@'localhost' IDENTIFIED BY 'bwma';
GRANT ALL PRIVILEGES ON *.* TO 'bwma'@'%';