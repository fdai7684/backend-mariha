# AdventureSphere - Backend


## Development Installation
1. Clone the repository
2. Run `npm install`
3. Run `npm run start:dev` which will run the application with nodemon for faster development
4. (optional) Run `npm run prepare` to configure husky and git hooks. Usually this is done automatically when running `npm install`.
   
> Note: Make sure to adhere to the commitlint guidelines for commiting, otherwise hooks might fail. An online helper can be found [here](https://commitlint.io/)

## Production Installation
1. Clone the repository
2. Run `npm install`
3. Run `npm run build` to compile typescript. If everything works you should find a new folder `dist` with the compiled javascript files for deployment.

## Documentation
- [Full Documentation](https://gitlab.informatik.hs-fulda.de/bwma-app/documentation) 
- [API Docs](https://gitlab.informatik.hs-fulda.de/bwma-app/documentation/-/blob/main/api/openapi.yml)
- [Database Docs](https://gitlab.informatik.hs-fulda.de/bwma-app/documentation/-/blob/main/database/schema.rdbm)
> Note: In order to view the database scheme copy the `schema.rdbm` file content into https://dbdiagram.io/d
- Database Docs [tbd]
- Frontend Docs [tbd]

## Commands
You can run all commands from the `package.json`, just preface it with `npm run <command>`. These are some basic explanations:

| Command     | Details |
|-------------|-----------|
| start:dev   | Run the application with `nodemon` for automatic reloading when applying changes in `src` folder. |
| build       | Build the TypeScript files to JavaScript. Output can be found in `dist` folder. |
| lint        | Highlight formatting issues using eslint. |
| format      | Try autoformatting files in `src`. Issues not being autoformatted will be printed. |
| test        | Run the tests in `tests` folder. |
| test:ci     | Run the tests in `tests` folder using a coverage reporter for Gitlab CI/CD and Badges. |
| prepare     | Runs automatically when using `npm i` or equivalent. Will install husky and apply git hooks in folder `.husky`. |
| migrate     | Apply database migrations using `knexjs` from folder `src/database/migrations` |
| rollback    | Rollback database migrations using `knexjs` from folder `src/database/migrations` |
| seed    | Apply database seeds using `knexjs` from folder `src/database/seeds` |
| git:update  | Update git submodules in this project |


## Others
tbd