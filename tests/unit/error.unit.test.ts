import { NextFunction } from 'express';
import {CustomErrorHandler, handleRequestError} from '../../src/services/error.service';

describe('Error service tests', () => {

    const err = new CustomErrorHandler(400, 'My error msg', 'My public error msg');

    it('Expect CustomErrorHandler to be of type error & CustomErrorHandler', () => {
        expect(err).toBeInstanceOf(Error);
        expect(err).toBeInstanceOf(CustomErrorHandler);
    })
    it('Should have public and normal error msg', () => {
        expect(err.message).not.toBe(undefined);
        expect(err.publicMessage).not.toBe(undefined);
    })

    let mockRequest: any;
    let mockResponse: any;
    let nextFunction: NextFunction;
    let defaultGetOptions = {
        limit: 100,
        offset: 0,
        sortBy: '',
        sortOrder: 'asc'
    }

    beforeEach(() => {
        mockRequest = { url: '/', method: 'GET', headers: {}, body: {}, query: {}, params: {} };
        mockResponse = {
            status: jest.fn((code) => {return mockResponse}),
            json: jest.fn().mockReturnValue(err),
            render: jest.fn().mockReturnValue(err),
            send: jest.fn().mockReturnValue(err),
        };
        nextFunction = jest.fn();
    });
    it('API error should be logged and response sent', () => {
        mockRequest.method = 'GET';
        handleRequestError(err, mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(err.code);
        expect(mockResponse.send).toHaveBeenCalledWith(err.publicMessage);
    })
    it('Should call NextFunction', () => {
        mockResponse.headersSent = true;
        handleRequestError(err, mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err.publicMessage);
    })
});
