import { CustomErrorHandler } from "../../src/services/error.service";
import { NextFunction } from "express";
import { CartService } from '../../src/services/cart.service'
import { CartRouter } from '../../src/controller/cart.controller'

describe('Carts-Service & Controller tests', () => {

    /**
    * SERVICE - TESTS
    */
    let mockRequest: any;
    let mockResponse: any;
    let nextFunction: NextFunction;
    const cartId = 5;
    let successMockDB: any;
    let failMockDB: any;
    let successMockService;
    let failMockService;
    const err = new CustomErrorHandler(500, 'my error', 'my public error')
    let defaultCart;

    beforeEach(() => {
        mockRequest = { headers: {}, body: {}, query: {}, params: {} };
        mockResponse = {
            json: jest.fn((msg) => { return msg }),
            send: jest.fn((msg) => { return msg }),
            status: jest.fn((code) => { return mockResponse }),
        };
        successMockDB = {
            getAllCartsDB: jest.fn(async (options) => []),
            createCartDB: jest.fn(async () => { return { affectedRows: 1, insertId: cartId } }),
            getCartByIdDB: jest.fn(async (cartId, options) => []),
            deleteCartByIdDB: jest.fn(async (cartId) => 1),
            updateCartByIdDB: jest.fn(async (cartId,) => 1)
        }
        failMockDB = {
            getAllCartsDB: jest.fn(async (options) => err),
            createCartDB: jest.fn(async () => err),
            getCartByIdDB: jest.fn(async (cartId, options) => err),
            deleteCartByIdDB: jest.fn(async (cartId) => err),
            updateCartByIdDB: jest.fn(async (cartId,) => err)
        }
        successMockService = new CartService(successMockDB);
        failMockService = new CartService(failMockDB);
        nextFunction = jest.fn();
        defaultCart = {}
    });

    it('"getAllCarts" should return an array', async () => {

        await successMockService.getAllCarts(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.send).toHaveBeenCalledWith([]);
        expect(successMockDB.getAllCartsDB).toHaveBeenCalledWith();
    });
    it('"getCartById" should return 200 and cartId', async () => {
        successMockDB.getCartByIdDB = jest.fn(async (cartId, options) => [cartId]),
        mockRequest.body = { cartId };
        await successMockService.getCartById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith(cartId);
        expect(successMockDB.getCartByIdDB).toHaveBeenCalledWith(cartId);
    });
    it('"getCartById" should return 204', async () => {
        mockRequest.body = { cartId };
        await successMockService.getCartById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(204);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.getCartByIdDB).toHaveBeenCalledWith(cartId,);
    });
    it('"createCart" should return 200', async () => {
        mockRequest.body = defaultCart;
        await successMockService.createCart(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith({ ...defaultCart, cartId });
        expect(successMockDB.createCartDB).toHaveBeenCalledWith();
    });
    it('"updateCartById" should return updated interface', async () => {
        mockRequest.body = { ...defaultCart, cartId };
        await successMockService.updateCartById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith({ ...defaultCart, cartId });
        expect(successMockDB.updateCartByIdDB).toHaveBeenCalledWith(cartId, );
    });

    it('"deleteCartById" should return 200', async () => {
        mockRequest.body = { cartId };
        await successMockService.deleteCartById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.deleteCartByIdDB).toHaveBeenCalledWith(cartId);
    });
    it('"deleteCartById" should return 204', async () => {
        successMockDB.deleteCartByIdDB = jest.fn(async (cartId) => 0),
        mockRequest.body = { cartId };
        await successMockService.deleteCartById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(204);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.deleteCartByIdDB).toHaveBeenCalledWith(cartId);
    });







    it('"getAllCarts" should return an array', async () => {
        await failMockService.getAllCarts(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);
    });
    it('"getCartById" should return 200 and cartId', async () => {
        successMockDB.getCartByIdDB = jest.fn(async (cartId, options) => [cartId]),
        mockRequest.body = { cartId };
        await failMockService.getCartById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"getCartById" should return 204', async () => {
        mockRequest.body = { cartId };
        await failMockService.getCartById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"createCart" should return 200', async () => {
        mockRequest.body = defaultCart;
        await failMockService.createCart(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"updateCartById" should return updated interface', async () => {
        mockRequest.body = { ...defaultCart, cartId };
        await failMockService.updateCartById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });

    it('"deleteCartById" should return 200', async () => {
        mockRequest.body = { cartId };
        await failMockService.deleteCartById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"deleteCartById" should return 204', async () => {
        successMockDB.deleteCartByIdDB = jest.fn(async (cartId) => 0),
        mockRequest.body = { cartId };
        await failMockService.deleteCartById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });


    /**
     * CONTROLLER - TESTS
     */

    const mockService = {
        db: successMockDB,
        getCartEvents: jest.fn(),
        getAllCarts: jest.fn(),
        createCart: jest.fn(),
        getCartById: jest.fn(),
        deleteCartById: jest.fn(),
        updateCartById: jest.fn(),
    }

})
