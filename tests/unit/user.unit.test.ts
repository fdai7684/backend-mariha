import { CustomErrorHandler } from "../../src/services/error.service";
import { NextFunction } from "express";
import { UserService } from '../../src/services/user.service'
import { UserRouter } from '../../src/controller/user.controller'

describe('Users-Service & Controller tests', () => {

    /**
    * SERVICE - TESTS
    */
    let mockRequest: any;
    let mockResponse: any;
    let nextFunction: NextFunction;
    const userId = '18731f0f-29ab-499c-b60c-c93a6f3f93bf';
    let successMockDB: any;
    let failMockDB: any;
    let successMockService;
    let failMockService;
    const err = new CustomErrorHandler(500, 'my error', 'my public error')
    let defaultUser;

    beforeEach(() => {
        mockRequest = { headers: {}, body: {}, query: {}, params: {}, keycloakToken: {sub: userId} };
        mockResponse = {
            json: jest.fn((msg) => { return msg }),
            send: jest.fn((msg) => { return msg }),
            status: jest.fn((code) => { return mockResponse }),
        };
        successMockDB = {
            getAllUsersDB: jest.fn(async (options) => []),
            createUserDB: jest.fn(async () => { return { affectedRows: 1, insertId: userId } }),
            getUserByIdDB: jest.fn(async (userId, options) => []),
            deleteUserByIdDB: jest.fn(async (userId) => 1),
            updateUserByIdDB: jest.fn(async (userId,) => 1)
        }
        failMockDB = {
            getAllUsersDB: jest.fn(async (options) => err),
            createUserDB: jest.fn(async () => err),
            getUserByIdDB: jest.fn(async (userId, options) => err),
            deleteUserByIdDB: jest.fn(async (userId) => err),
            updateUserByIdDB: jest.fn(async (userId,) => err)
        }
        successMockService = new UserService(successMockDB);
        failMockService = new UserService(failMockDB);
        nextFunction = jest.fn();
        defaultUser = {email: "max@muster.de", role: "customer", password: "", salt: "", firstName: "Max", lastName: "Muster", street: "Musterstrasse", housenumber: 123, additionalAddress: "", postcode: "12345", city: "Musterstadt", country: "Germany"}
    });

    it('"getAllUsers" should return an array', async () => {

        await successMockService.getAllUsers(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.send).toHaveBeenCalledWith([]);
        expect(successMockDB.getAllUsersDB).toHaveBeenCalledWith();
    });
    it('"getUserById" should return 200 and userId', async () => {
        successMockDB.getUserByIdDB = jest.fn(async (userId, options) => [userId]),
        await successMockService.getUserById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith(userId);
        expect(successMockDB.getUserByIdDB).toHaveBeenCalledWith(userId);
    });
    it('"getUserById" should return 204', async () => {
        await successMockService.getUserById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(204);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.getUserByIdDB).toHaveBeenCalledWith(userId);
    });
    it('"createUser" should return 200', async () => {
        mockRequest.body = defaultUser;
        await successMockService.createUser(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith({ ...defaultUser, userId });
        expect(successMockDB.createUserDB).toHaveBeenCalledWith(defaultUser.email, defaultUser.role, defaultUser.password, defaultUser.salt, defaultUser.firstName, defaultUser.lastName, defaultUser.street, defaultUser.housenumber, defaultUser.additionalAddress, defaultUser.postcode, defaultUser.city, defaultUser.country);
    });
    it('"updateUserById" should return updated interface', async () => {
        const modifiedUser: any = { ...defaultUser }
        delete modifiedUser.password;
        delete modifiedUser.salt;
        delete modifiedUser.email;
        delete modifiedUser.role;
        mockRequest.body = { ...defaultUser, userId };
        await successMockService.updateUserById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith({ ...modifiedUser, userId });
        expect(successMockDB.updateUserByIdDB).toHaveBeenCalledWith(userId, defaultUser.firstName, defaultUser.lastName, defaultUser.street, defaultUser.housenumber, defaultUser.additionalAddress, defaultUser.postcode, defaultUser.city, defaultUser.country);
    });

    it('"deleteUserById" should return 200', async () => {
        mockRequest.body = { userId };
        await successMockService.deleteUserById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.deleteUserByIdDB).toHaveBeenCalledWith(userId);
    });
    it('"deleteUserById" should return 204', async () => {
        successMockDB.deleteUserByIdDB = jest.fn(async (userId) => 0),
        mockRequest.body = { userId };
        await successMockService.deleteUserById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(204);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.deleteUserByIdDB).toHaveBeenCalledWith(userId);
    });







    it('"getAllUsers" should return an array', async () => {
        await failMockService.getAllUsers(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);
    });
    it('"getUserById" should return 200 and userId', async () => {
        successMockDB.getUserByIdDB = jest.fn(async (userId, options) => [userId]),
        mockRequest.body = { userId };
        await failMockService.getUserById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"getUserById" should return 204', async () => {
        mockRequest.body = { userId };
        await failMockService.getUserById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"createUser" should return 200', async () => {
        mockRequest.body = defaultUser;
        await failMockService.createUser(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"updateUserById" should return updated interface', async () => {
        mockRequest.body = { ...defaultUser, userId };
        await failMockService.updateUserById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });

    it('"deleteUserById" should return 200', async () => {
        mockRequest.body = { userId };
        await failMockService.deleteUserById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"deleteUserById" should return 204', async () => {
        successMockDB.deleteUserByIdDB = jest.fn(async (userId) => 0),
        mockRequest.body = { userId };
        await failMockService.deleteUserById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });


    /**
     * CONTROLLER - TESTS
     */

    const mockService = {
        db: successMockDB,
        getUserEvents: jest.fn(),
        getAllUsers: jest.fn(),
        createUser: jest.fn(),
        getUserById: jest.fn(),
        deleteUserById: jest.fn(),
        updateUserById: jest.fn(),
    }

})
