import { NextFunction } from "express";
import * as Joi from "joi";
import { dateTimeValidationHelper, schemas, updateSchemas } from '../../src/services/validator.service'
import { API_GET_OPTIONS_SCHEME } from '../../src/models/api/api.schema'
import { validateRequest, extractURLWithoutParams, dateValidationHelper } from '../../src/services/validator.service'

describe('Validator service tests', () => {

    let mockRequest: any;
    let mockResponse: any;
    let nextFunction: NextFunction;
    schemas['/experiments'] = {
        'GET': Joi.object({
            options: API_GET_OPTIONS_SCHEME
        })
    };

    beforeEach(() => {
        mockRequest = { url: '/experiments', method: 'GET', headers: {}, body: {}, query: {}, params: {} };
        mockResponse = {
            json: jest.fn()
        };
        nextFunction = jest.fn();
    });

    it('Should throw an error because no scheme for url is defined', () => {
        mockRequest.url = ''
        validateRequest(mockRequest, mockResponse, nextFunction)
        expect(nextFunction).toHaveBeenCalledTimes(1);
        expect(nextFunction).toHaveBeenCalledWith(Error('Missing Joi schema for url or request method'));
    })
    it('Should throw an error because method scheme for url is not defined', () => {
        mockRequest.method = 'POST'
        validateRequest(mockRequest, mockResponse, nextFunction)
        expect(nextFunction).toHaveBeenCalledTimes(1);
        expect(nextFunction).toHaveBeenCalledWith(Error('Missing Joi schema for url or request method'));
    })
    it('Should throw an error when get-options limit < 0', () => {
        mockRequest.body = { options: { limit: -1 } };
        validateRequest(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledTimes(1);
        expect(nextFunction).toHaveBeenCalledWith(Error('Missing or incorrect request input:\nValidationError: "options.limit" must be greater than or equal to 1'))
    })
    it('Should throw an error when get-options offset < 0', () => {
        mockRequest.body = { options: { offset: -1 } };
        validateRequest(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledTimes(1);
        expect(nextFunction).toHaveBeenCalledWith(Error('Missing or incorrect request input:\nValidationError: "options.offset" must be greater than or equal to 0'))
    })
    it('Should throw an error when get-options sortBy is not a string', () => {
        mockRequest.body = { options: { sortBy: 1 } };
        validateRequest(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledTimes(1);
        expect(nextFunction).toHaveBeenCalledWith(Error('Missing or incorrect request input:\nValidationError: "options.sortBy" must be a string'))
    })
    it('Should throw an error when get-options sortBy is empty', () => {
        mockRequest.body = { options: { sortBy: "" } };
        validateRequest(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledTimes(1);
        expect(nextFunction).toHaveBeenCalledWith(Error('Missing or incorrect request input:\nValidationError: "options.sortBy" is not allowed to be empty'))
    })
    it('Should throw an error when get-options sortOrder is not "asc" or "desc"', () => {
        mockRequest.body = { options: { sortOrder: "up" } };
        validateRequest(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledTimes(1);
        expect(nextFunction).toHaveBeenCalledWith(Error('Missing or incorrect request input:\nValidationError: "options.sortOrder" must be one of [asc, desc]'))
    })
    it('Should throw an error when get-options sortOrder is empty', () => {
        mockRequest.body = { options: { sortOrder: "" } };
        validateRequest(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledTimes(1);
        expect(nextFunction).toHaveBeenCalledWith(Error('Missing or incorrect request input:\nValidationError: "options.sortOrder" must be one of [asc, desc]'))
    })
    it('Should create a get-options object with default values', () => {
        mockRequest.body.options = {};
        validateRequest(mockRequest, mockResponse, nextFunction);
        expect(mockRequest.body.options).toEqual({ limit: 100, offset: 0, sortOrder: 'asc' });
    })

    it('Should return URL without params', () => {
        const url = 'https://cfc.informatik.hs-fulda.de/morgana/experiment/3/checklist/1';
        const params = { experimentId: 3, checklistId: 1 };
        const result = extractURLWithoutParams(url, params);
        expect(result).toEqual('https://cfc.informatik.hs-fulda.de/morgana/experiment/checklist');
    })
    it('Should validate birthday without changing the string if correct', () => {
        const birthday = '1999-01-01';
        const helper: any = { original: birthday, }
        const result = dateValidationHelper(birthday, helper);
        expect(birthday).toEqual(result);
    })
    it('Should throw a validation error for wrong birthday format', () => {
        const birthday = '01-01-1999';
        const helper: any = { error: jest.fn() }
        dateValidationHelper(birthday, helper);
        expect(helper.error).toHaveBeenCalled();
    })
    it('Should validate the dateTime and format it', () => {
        const dateTime = new Date('1999-01-01T20:22:00.000').toString();
        const helper: any = { error: jest.fn() }
        const result = dateTimeValidationHelper(dateTime, helper);
        expect(result).toEqual('1999-01-01 20:22:00')
    })
    it('Should throw a validation error for wrong date', () => {
        const dateTime = new Date('1999-21-01T20:22:00.000').toString();
        const helper: any = { error: jest.fn() }
        const result = dateTimeValidationHelper(dateTime, helper);
        expect(helper.error).toHaveBeenCalled()
    })

    
    /**
     * SCHEME TESTS
     */
    describe('Validator tests', () => {

        it('Expect schemas to have 13 routes', async () => {
            await updateSchemas();
            expect(Object.keys(schemas).length).toEqual(13);
        })

    })
});
