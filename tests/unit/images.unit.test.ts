import { CustomErrorHandler } from "../../src/services/error.service";
import { NextFunction } from "express";
import { ImagesService } from '../../src/services/images.service'
import { ImageRouter } from '../../src/controller/images.controller'

describe('Images-Service & Controller tests', () => {

    /**
    * SERVICE - TESTS
    */
    let mockRequest: any;
    let mockResponse: any;
    let nextFunction: NextFunction;
    const imageId = 5;
    let successMockDB: any;
    let failMockDB: any;
    let successMockService;
    let failMockService;
    let url = "https://www.google.de"
    const err = new CustomErrorHandler(500, 'my error', 'my public error')
    let defaultImage;

    beforeEach(() => {
        mockRequest = { headers: {}, body: {}, query: {}, params: {} };
        mockResponse = {
            json: jest.fn((msg) => { return msg }),
            send: jest.fn((msg) => { return msg }),
            status: jest.fn((code) => { return mockResponse }),
        };
        successMockDB = {
            getAllImagesDB: jest.fn(async (options) => []),
            createImageDB: jest.fn(async (url) => { return { affectedRows: 1, insertId: imageId } }),
            getImageByIdDB: jest.fn(async (imageId) => []),
            deleteImageByIdDB: jest.fn(async (imageId) => 1),
            updateImageByIdDB: jest.fn(async (imageId) => 1)
        }
        failMockDB = {
            getAllImagesDB: jest.fn(async (options) => err),
            createImageDB: jest.fn(async (url) => err),
            getImageByIdDB: jest.fn(async (imageId, options) => err),
            deleteImageByIdDB: jest.fn(async (imageId) => err),
            updateImageByIdDB: jest.fn(async (imageId) => err)
        }
        successMockService = new ImagesService(successMockDB);
        failMockService = new ImagesService(failMockDB);
        nextFunction = jest.fn();
        defaultImage = {url}
    });

    it('"getAllImages" should return an array', async () => {

        await successMockService.getAllImages(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.send).toHaveBeenCalledWith([]);
        expect(successMockDB.getAllImagesDB).toHaveBeenCalledWith();
    });
    it('"getImageById" should return 200 and imageId', async () => {
        successMockDB.getImageByIdDB = jest.fn(async (imageId, options) => [imageId]),
        mockRequest.body = { imageId };
        await successMockService.getImageById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith(imageId);
        expect(successMockDB.getImageByIdDB).toHaveBeenCalledWith(imageId);
    });
    it('"getImageById" should return 204', async () => {
        mockRequest.body = { imageId };
        await successMockService.getImageById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(204);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.getImageByIdDB).toHaveBeenCalledWith(imageId,);
    });
    it('"createImage" should return 200', async () => {
        mockRequest.body = defaultImage;
        await successMockService.createImage(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith({ ...defaultImage, imagesId: imageId });
        expect(successMockDB.createImageDB).toHaveBeenCalledWith(url);
    });
    it('"updateImageById" should return updated interface', async () => {
        mockRequest.body = { imageId, url };
        await successMockService.updateImageById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith({  imageId, url });
        expect(successMockDB.updateImageByIdDB).toHaveBeenCalledWith(imageId, url);
    });

    it('"deleteImageById" should return 200', async () => {
        mockRequest.body = { imageId };
        await successMockService.deleteImageById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.deleteImageByIdDB).toHaveBeenCalledWith(imageId);
    });
    it('"deleteImageById" should return 204', async () => {
        successMockDB.deleteImageByIdDB = jest.fn(async (imageId) => 0),
        mockRequest.body = { imageId };
        await successMockService.deleteImageById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(204);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.deleteImageByIdDB).toHaveBeenCalledWith(imageId);
    });







    it('"getAllImages" should return an array', async () => {
        await failMockService.getAllImages(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);
    });
    it('"getImageById" should return 200 and imageId', async () => {
        successMockDB.getImageByIdDB = jest.fn(async (imageId, options) => [imageId]),
        mockRequest.body = { imageId };
        await failMockService.getImageById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"getImageById" should return 204', async () => {
        mockRequest.body = { imageId };
        await failMockService.getImageById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"createImage" should return 200', async () => {
        mockRequest.body = defaultImage;
        await failMockService.createImage(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"updateImageById" should return updated interface', async () => {
        mockRequest.body = { ...defaultImage, imageId };
        await failMockService.updateImageById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });

    it('"deleteImageById" should return 200', async () => {
        mockRequest.body = { imageId };
        await failMockService.deleteImageById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"deleteImageById" should return 204', async () => {
        successMockDB.deleteImageByIdDB = jest.fn(async (imageId) => 0),
        mockRequest.body = { imageId };
        await failMockService.deleteImageById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });


    /**
     * CONTROLLER - TESTS
     */

    const mockService = {
        db: successMockDB,
        getImageEvents: jest.fn(),
        getAllImages: jest.fn(),
        createImage: jest.fn(),
        getImageById: jest.fn(),
        deleteImageById: jest.fn(),
        updateImageById: jest.fn(),
    }
})
