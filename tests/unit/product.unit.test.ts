import { CustomErrorHandler } from "../../src/services/error.service";
import { NextFunction } from "express";
import { ProductService } from '../../src/services/product.service'
import { ProductRouter } from '../../src/controller/product.controller'

describe('Products-Service & Controller tests', () => {

    /**
    * SERVICE - TESTS
    */
    let mockRequest: any;
    let mockResponse: any;
    let nextFunction: NextFunction;
    const productId = 5;
    let successMockDB: any;
    let failMockDB: any;
    let successMockService;
    let failMockService;
    const err = new CustomErrorHandler(500, 'my error', 'my public error')
    let defaultProduct;

    beforeEach(() => {
        mockRequest = { headers: {}, body: {}, query: {}, params: {} };
        mockResponse = {
            json: jest.fn((msg) => { return msg }),
            send: jest.fn((msg) => { return msg }),
            status: jest.fn((code) => { return mockResponse }),
        };
        successMockDB = {
            getAllProductsDB: jest.fn(async (options) => []),
            createProductDB: jest.fn(async () => { return { affectedRows: 1, insertId: productId } }),
            getProductByIdDB: jest.fn(async (productId, options) => []),
            deleteProductByIdDB: jest.fn(async (productId) => 1),
            updateProductByIdDB: jest.fn(async (productId,) => 1)
        }
        failMockDB = {
            getAllProductsDB: jest.fn(async (options) => err),
            createProductDB: jest.fn(async () => err),
            getProductByIdDB: jest.fn(async (productId, options) => err),
            deleteProductByIdDB: jest.fn(async (productId) => err),
            updateProductByIdDB: jest.fn(async (productId,) => err)
        }
        successMockService = new ProductService(successMockDB);
        failMockService = new ProductService(failMockDB);
        nextFunction = jest.fn();
        defaultProduct = {price: 12.45, amount: 3}
    });

    it('"getAllProducts" should return an array', async () => {

        await successMockService.getAllProducts(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.send).toHaveBeenCalledWith([]);
        expect(successMockDB.getAllProductsDB).toHaveBeenCalledWith();
    });
    it('"getProductById" should return 200 and productId', async () => {
        successMockDB.getProductByIdDB = jest.fn(async (productId, options) => [productId]),
        mockRequest.body = { productId };
        await successMockService.getProductById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith(productId);
        expect(successMockDB.getProductByIdDB).toHaveBeenCalledWith(productId);
    });
    it('"getProductById" should return 204', async () => {
        mockRequest.body = { productId };
        await successMockService.getProductById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(204);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.getProductByIdDB).toHaveBeenCalledWith(productId,);
    });
    it('"createProduct" should return 200', async () => {
        mockRequest.body = defaultProduct;
        await successMockService.createProduct(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith({ ...defaultProduct, productId });
        expect(successMockDB.createProductDB).toHaveBeenCalledWith(defaultProduct.price, defaultProduct.amount);
    });
    it('"updateProductById" should return updated interface', async () => {
        mockRequest.body = { ...defaultProduct, productId };
        await successMockService.updateProductById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith({ ...defaultProduct, productId });
        expect(successMockDB.updateProductByIdDB).toHaveBeenCalledWith(productId, defaultProduct.price, defaultProduct.amount);
    });

    it('"deleteProductById" should return 200', async () => {
        mockRequest.body = { productId };
        await successMockService.deleteProductById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.deleteProductByIdDB).toHaveBeenCalledWith(productId);
    });
    it('"deleteProductById" should return 204', async () => {
        successMockDB.deleteProductByIdDB = jest.fn(async (productId) => 0),
        mockRequest.body = { productId };
        await successMockService.deleteProductById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(204);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.deleteProductByIdDB).toHaveBeenCalledWith(productId);
    });







    it('"getAllProducts" should return an array', async () => {
        await failMockService.getAllProducts(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);
    });
    it('"getProductById" should return 200 and productId', async () => {
        successMockDB.getProductByIdDB = jest.fn(async (productId, options) => [productId]),
        mockRequest.body = { productId };
        await failMockService.getProductById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"getProductById" should return 204', async () => {
        mockRequest.body = { productId };
        await failMockService.getProductById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"createProduct" should return 200', async () => {
        mockRequest.body = defaultProduct;
        await failMockService.createProduct(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"updateProductById" should return updated interface', async () => {
        mockRequest.body = { ...defaultProduct, productId };
        await failMockService.updateProductById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });

    it('"deleteProductById" should return 200', async () => {
        mockRequest.body = { productId };
        await failMockService.deleteProductById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"deleteProductById" should return 204', async () => {
        successMockDB.deleteProductByIdDB = jest.fn(async (productId) => 0),
        mockRequest.body = { productId };
        await failMockService.deleteProductById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });


    /**
     * CONTROLLER - TESTS
     */

    const mockService = {
        db: successMockDB,
        getProductEvents: jest.fn(),
        getAllProducts: jest.fn(),
        createProduct: jest.fn(),
        getProductById: jest.fn(),
        deleteProductById: jest.fn(),
        updateProductById: jest.fn(),
    }

})
