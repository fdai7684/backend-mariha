import { CustomErrorHandler } from "../../src/services/error.service";
import { NextFunction } from "express";
import { WishlistService } from '../../src/services/wishlist.service'
import { WishlistRouter } from '../../src/controller/wishlist.controller'

describe('Wishlists-Service & Controller tests', () => {

    /**
    * SERVICE - TESTS
    */
    let mockRequest: any;
    let mockResponse: any;
    let nextFunction: NextFunction;
    const wishlistId = 5;
    let successMockDB: any;
    let failMockDB: any;
    let successMockService;
    let failMockService;
    const err = new CustomErrorHandler(500, 'my error', 'my public error')
    let defaultWishlist;

    beforeEach(() => {
        mockRequest = { headers: {}, body: {}, query: {}, params: {} };
        mockResponse = {
            json: jest.fn((msg) => { return msg }),
            send: jest.fn((msg) => { return msg }),
            status: jest.fn((code) => { return mockResponse }),
        };
        successMockDB = {
            getAllWishlistsDB: jest.fn(async (options) => []),
            createWishlistDB: jest.fn(async () => { return { affectedRows: 1, insertId: wishlistId } }),
            getWishlistByIdDB: jest.fn(async (wishlistId, options) => []),
            deleteWishlistByIdDB: jest.fn(async (wishlistId) => 1),
            updateWishlistByIdDB: jest.fn(async (wishlistId,) => 1)
        }
        failMockDB = {
            getAllWishlistsDB: jest.fn(async (options) => err),
            createWishlistDB: jest.fn(async () => err),
            getWishlistByIdDB: jest.fn(async (wishlistId, options) => err),
            deleteWishlistByIdDB: jest.fn(async (wishlistId) => err),
            updateWishlistByIdDB: jest.fn(async (wishlistId,) => err)
        }
        successMockService = new WishlistService(successMockDB);
        failMockService = new WishlistService(failMockDB);
        nextFunction = jest.fn();
        defaultWishlist = {}
    });

    it('"getAllWishlists" should return an array', async () => {

        await successMockService.getAllWishlists(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.send).toHaveBeenCalledWith([]);
        expect(successMockDB.getAllWishlistsDB).toHaveBeenCalledWith();
    });
    it('"getWishlistById" should return 200 and wishlistId', async () => {
        successMockDB.getWishlistByIdDB = jest.fn(async (wishlistId, options) => [wishlistId]),
        mockRequest.body = { wishlistId };
        await successMockService.getWishlistById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith(wishlistId);
        expect(successMockDB.getWishlistByIdDB).toHaveBeenCalledWith(wishlistId);
    });
    it('"getWishlistById" should return 204', async () => {
        mockRequest.body = { wishlistId };
        await successMockService.getWishlistById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(204);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.getWishlistByIdDB).toHaveBeenCalledWith(wishlistId,);
    });
    it('"createWishlist" should return 200', async () => {
        mockRequest.body = defaultWishlist;
        await successMockService.createWishlist(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith({ ...defaultWishlist, wishlistId });
        expect(successMockDB.createWishlistDB).toHaveBeenCalledWith();
    });
    it('"updateWishlistById" should return updated interface', async () => {
        mockRequest.body = { ...defaultWishlist, wishlistId };
        await successMockService.updateWishlistById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith({ ...defaultWishlist, wishlistId });
        expect(successMockDB.updateWishlistByIdDB).toHaveBeenCalledWith(wishlistId, );
    });

    it('"deleteWishlistById" should return 200', async () => {
        mockRequest.body = { wishlistId };
        await successMockService.deleteWishlistById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.deleteWishlistByIdDB).toHaveBeenCalledWith(wishlistId);
    });
    it('"deleteWishlistById" should return 204', async () => {
        successMockDB.deleteWishlistByIdDB = jest.fn(async (wishlistId) => 0),
        mockRequest.body = { wishlistId };
        await successMockService.deleteWishlistById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(204);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.deleteWishlistByIdDB).toHaveBeenCalledWith(wishlistId);
    });







    it('"getAllWishlists" should return an array', async () => {
        await failMockService.getAllWishlists(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);
    });
    it('"getWishlistById" should return 200 and wishlistId', async () => {
        successMockDB.getWishlistByIdDB = jest.fn(async (wishlistId, options) => [wishlistId]),
        mockRequest.body = { wishlistId };
        await failMockService.getWishlistById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"getWishlistById" should return 204', async () => {
        mockRequest.body = { wishlistId };
        await failMockService.getWishlistById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"createWishlist" should return 200', async () => {
        mockRequest.body = defaultWishlist;
        await failMockService.createWishlist(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"updateWishlistById" should return updated interface', async () => {
        mockRequest.body = { ...defaultWishlist, wishlistId };
        await failMockService.updateWishlistById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });

    it('"deleteWishlistById" should return 200', async () => {
        mockRequest.body = { wishlistId };
        await failMockService.deleteWishlistById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"deleteWishlistById" should return 204', async () => {
        successMockDB.deleteWishlistByIdDB = jest.fn(async (wishlistId) => 0),
        mockRequest.body = { wishlistId };
        await failMockService.deleteWishlistById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });


    /**
     * CONTROLLER - TESTS
     */

    const mockService = {
        db: successMockDB,
        getWishlistEvents: jest.fn(),
        getAllWishlists: jest.fn(),
        createWishlist: jest.fn(),
        getWishlistById: jest.fn(),
        deleteWishlistById: jest.fn(),
        updateWishlistById: jest.fn(),
    }

})
