import { CustomErrorHandler } from "../../src/services/error.service";
import { NextFunction } from "express";
import { OrderService } from '../../src/services/order.service'
import { OrderRouter } from '../../src/controller/order.controller'

describe('Orders-Service & Controller tests', () => {

    /**
    * SERVICE - TESTS
    */
    let mockRequest: any;
    let mockResponse: any;
    let nextFunction: NextFunction;
    const orderId = 5;
    let successMockDB: any;
    let failMockDB: any;
    let successMockService;
    let failMockService;
    const err = new CustomErrorHandler(500, 'my error', 'my public error')
    let defaultOrder;

    beforeEach(() => {
        mockRequest = { headers: {}, body: {}, query: {}, params: {} };
        mockResponse = {
            json: jest.fn((msg) => { return msg }),
            send: jest.fn((msg) => { return msg }),
            status: jest.fn((code) => { return mockResponse }),
        };
        successMockDB = {
            getAllOrdersDB: jest.fn(async (options) => []),
            createOrderDB: jest.fn(async () => { return { affectedRows: 1, insertId: orderId } }),
            getOrderByIdDB: jest.fn(async (orderId, options) => []),
            deleteOrderByIdDB: jest.fn(async (orderId) => 1),
            updateOrderByIdDB: jest.fn(async (orderId,) => 1)
        }
        failMockDB = {
            getAllOrdersDB: jest.fn(async (options) => err),
            createOrderDB: jest.fn(async () => err),
            getOrderByIdDB: jest.fn(async (orderId, options) => err),
            deleteOrderByIdDB: jest.fn(async (orderId) => err),
            updateOrderByIdDB: jest.fn(async (orderId,) => err)
        }
        successMockService = new OrderService(successMockDB);
        failMockService = new OrderService(failMockDB);
        nextFunction = jest.fn();
        defaultOrder = {paymentMethod: "visa", address: "musterstrasse 123, 12345 Musterstadt", complete: false}
    });

    it('"getAllOrders" should return an array', async () => {

        await successMockService.getAllOrders(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.send).toHaveBeenCalledWith([]);
        expect(successMockDB.getAllOrdersDB).toHaveBeenCalledWith();
    });
    it('"getOrderById" should return 200 and orderId', async () => {
        successMockDB.getOrderByIdDB = jest.fn(async (orderId, options) => [orderId]),
        mockRequest.body = { orderId };
        await successMockService.getOrderById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith(orderId);
        expect(successMockDB.getOrderByIdDB).toHaveBeenCalledWith(orderId);
    });
    it('"getOrderById" should return 204', async () => {
        mockRequest.body = { orderId };
        await successMockService.getOrderById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(204);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.getOrderByIdDB).toHaveBeenCalledWith(orderId,);
    });
    it('"createOrder" should return 200', async () => {
        mockRequest.body = defaultOrder;
        await successMockService.createOrder(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith({ ...defaultOrder, orderId });
        expect(successMockDB.createOrderDB).toHaveBeenCalledWith(defaultOrder.paymentMethod, defaultOrder.address, defaultOrder.complete);
    });
    it('"updateOrderById" should return updated interface', async () => {
        mockRequest.body = { ...defaultOrder, orderId };
        await successMockService.updateOrderById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith({ ...defaultOrder, orderId });
        expect(successMockDB.updateOrderByIdDB).toHaveBeenCalledWith(orderId, defaultOrder.paymentMethod, defaultOrder.address, defaultOrder.complete);
    });

    it('"deleteOrderById" should return 200', async () => {
        mockRequest.body = { orderId };
        await successMockService.deleteOrderById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.deleteOrderByIdDB).toHaveBeenCalledWith(orderId);
    });
    it('"deleteOrderById" should return 204', async () => {
        successMockDB.deleteOrderByIdDB = jest.fn(async (orderId) => 0),
        mockRequest.body = { orderId };
        await successMockService.deleteOrderById(mockRequest, mockResponse, nextFunction);
        expect(mockResponse.status).toHaveBeenCalledWith(204);
        expect(mockResponse.send).toHaveBeenCalled();
        expect(successMockDB.deleteOrderByIdDB).toHaveBeenCalledWith(orderId);
    });







    it('"getAllOrders" should return an array', async () => {
        await failMockService.getAllOrders(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);
    });
    it('"getOrderById" should return 200 and orderId', async () => {
        successMockDB.getOrderByIdDB = jest.fn(async (orderId, options) => [orderId]),
        mockRequest.body = { orderId };
        await failMockService.getOrderById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"getOrderById" should return 204', async () => {
        mockRequest.body = { orderId };
        await failMockService.getOrderById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"createOrder" should return 200', async () => {
        mockRequest.body = defaultOrder;
        await failMockService.createOrder(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"updateOrderById" should return updated interface', async () => {
        mockRequest.body = { ...defaultOrder, orderId };
        await failMockService.updateOrderById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });

    it('"deleteOrderById" should return 200', async () => {
        mockRequest.body = { orderId };
        await failMockService.deleteOrderById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });
    it('"deleteOrderById" should return 204', async () => {
        successMockDB.deleteOrderByIdDB = jest.fn(async (orderId) => 0),
        mockRequest.body = { orderId };
        await failMockService.deleteOrderById(mockRequest, mockResponse, nextFunction);
        expect(nextFunction).toHaveBeenCalledWith(err);

    });


    /**
     * CONTROLLER - TESTS
     */

    const mockService = {
        db: successMockDB,
        getOrderEvents: jest.fn(),
        getAllOrders: jest.fn(),
        createOrder: jest.fn(),
        getOrderById: jest.fn(),
        deleteOrderById: jest.fn(),
        updateOrderById: jest.fn(),
    }
})
