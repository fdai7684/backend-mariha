import { kcConfig } from '../../src/config/keycloak';
import { config } from '../../src/config/config';

describe('Config tests', () => {
    const configKeys = ['APP_NAME', 'VERSION', 'HOST', 'BACKEND_PORT', 'FRONTEND_URL', 'DB_HOST', 'DB_USER', 'DB_PASSWORD', 'DB_NAME', 'DB_PORT', 'DB_CONNECTION_LIMIT', 'KEYCLOAK_AUTH_SERVER_URL', 'KEYCLOAK_SECRET', 'LOG_FOLDER_PATH', 'NODE_ENV']
    const keycloakConfigKeys = ["realm", "auth-server-url", "ssl-required", "resource", "credentials", "confidential-port"]
    it('Expect config keys to match config', () => {
        const configSorted = Object.keys(config).sort();
        const keysSorted = configKeys.sort();
        expect(configSorted).toEqual(keysSorted);
    })
    it('Expect keycloak-config keys to match keycloak-config', () => {
        const configSorted = Object.keys(kcConfig).sort();
        const keysSorted = keycloakConfigKeys.sort();
        expect(configSorted).toEqual(keysSorted);
    })

});
